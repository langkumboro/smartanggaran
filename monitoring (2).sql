-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2019 at 03:27 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `monitoring`
--
CREATE DATABASE IF NOT EXISTS `monitoring` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `monitoring`;

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE `kegiatan` (
  `kodekegiatan` varchar(200) NOT NULL,
  `tgl_pel` date NOT NULL,
  `lokasi` varchar(200) NOT NULL,
  `judulkegiatan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`kodekegiatan`, `tgl_pel`, `lokasi`, `judulkegiatan`) VALUES
('000001', '2019-09-12', 'Pasaman Barat', 'Penanaman alat deteksi cuaca'),
('000002', '2019-09-10', 'Pandeglang - Banten', 'Sosialisasi Apk Nelayan Go Online'),
('000003', '2019-09-19', 'Lebak - Banten', 'Sosialisasi Apk Petani Go Online'),
('000004', '2019-09-11', 'Muncang, Lebak, Banten', 'Penanaman alat deteksi cuaca di sektor pertanian');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id_user` int(11) NOT NULL,
  `akun` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `flag` int(11) NOT NULL,
  `tipe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id_user`, `akun`, `password`, `flag`, `tipe`) VALUES
(7, 'ADM-Langgeng', '39b49badfb31205cc2149931d0a45de1', 1, 1),
(8, 'PSW-ucok siantar', '4f797bf388435c24f5de74a8343eeb2d', 1, 2),
(9, 'ADM-muhrin', 'c3712e971f4c502a5da2a72787eaadb1', 1, 1),
(10, 'ADM-langgeng.k', '0192023a7bbd73250516f069df18b500', 1, 1),
(11, 'cuang', '7cb166f29ff452c6eab1a27bbeceb88e', 1, 2),
(12, 'Adnan', '2c9ebc5921116ff2263a35b95492e276', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `pagu`
--

CREATE TABLE `pagu` (
  `akun` varchar(200) NOT NULL,
  `kode` varchar(200) NOT NULL,
  `nama_program` varchar(200) NOT NULL,
  `jml_pagu` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pagu`
--

INSERT INTO `pagu` (`akun`, `kode`, `nama_program`, `jml_pagu`) VALUES
('521211-Bahan', '4138.006.054', 'fasilitas onboarding', 5000000),
('521211-Fasilitas', '4138.006.054', 'fasilitas onboarding', 45600000),
('52190-Fasilitas', '4138.06.054', 'fasilitas onboarding', 890000),
('522151-Fasilitas', '4138.007.054', 'sektor strategi perikanan', 3000000),
('524119-Paket', '4138.005.054', 'sektor strategis pertanian', 4000000),
('56211-Fasilitas', '4138.007.055', 'sektor strategi perikanan', 8000000),
('783800-Fasilitas nelayan', '4138.909.097', 'Sektor kapal nelayan', 908000000),
('78392-Bahan Baku', '4138.909.98', 'Sektor SDM pertanian', 78000000),
('Q3546', '4138.909.98', 'Sektor SDM pertanian', 7000000);

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `kode` varchar(200) NOT NULL,
  `nama_program` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`kode`, `nama_program`) VALUES
('4138.005.054', 'sektor strategis pertanian'),
('4138.006.054', 'fasilitas onboarding'),
('4138.007.054', 'sektor strategi perikanan'),
('4138.007.055', 'penanaman alat cuaca petani'),
('4138.909.98', 'Sektor SDM pertanian'),
('421.908.829', 'Peremajaan Sistem Petani Go Online');

-- --------------------------------------------------------

--
-- Table structure for table `serapan`
--

CREATE TABLE `serapan` (
  `id_d_serapan` int(11) NOT NULL,
  `nospp` varchar(20) NOT NULL,
  `akun` varchar(200) NOT NULL,
  `kodekegiatan` varchar(200) NOT NULL,
  `jenis_perwab` varchar(150) NOT NULL,
  `tgl_spp` date NOT NULL,
  `uraian_pek` varchar(200) NOT NULL,
  `nilai_kwit` decimal(10,0) NOT NULL,
  `ppn` varchar(200) NOT NULL,
  `kode` varchar(200) NOT NULL,
  `pph21` varchar(200) NOT NULL,
  `pph22` varchar(200) NOT NULL,
  `pph23` varchar(200) NOT NULL,
  `status` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `serapan`
--

INSERT INTO `serapan` (`id_d_serapan`, `nospp`, `akun`, `kodekegiatan`, `jenis_perwab`, `tgl_spp`, `uraian_pek`, `nilai_kwit`, `ppn`, `kode`, `pph21`, `pph22`, `pph23`, `status`) VALUES
(18, '235.78SD', '908 - Sumber Daya Manusia', '000003', 'LS', '2019-09-12', 'dalam rangka pengembangan ilmu di sektor pertanian bahwa kita ingin mengubah pola pikir petani untuk lebih maju kedepan lagi', '13500000', '10', '4138.909.98', '2', '0', '0', 'DISETUJUI'),
(19, 'SP378.980', '126 - Alat (Fasilitas)', '000004', 'GU', '2019-09-19', 'y', '7800000', '10%', '4138.007.055', '0%', '0%', '2,5%', 'DRAFT');

-- --------------------------------------------------------

--
-- Table structure for table `serapan_h`
--

CREATE TABLE `serapan_h` (
  `id_h_serapan` varchar(50) NOT NULL,
  `kode` varchar(200) NOT NULL,
  `nama_program` varchar(200) NOT NULL,
  `tanggal` date NOT NULL,
  `akun` varchar(200) NOT NULL,
  `realisasi` float NOT NULL,
  `sisa_dana` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `serapan_h`
--

INSERT INTO `serapan_h` (`id_h_serapan`, `kode`, `nama_program`, `tanggal`, `akun`, `realisasi`, `sisa_dana`, `flag`) VALUES
('LBK987', '4138.909.98', '4138.909.98', '2019-09-12', '549 - Fasilitas', 13500000, 1000000, 1),
('TR65', '4138.909.98', '4138.909.98', '2019-09-12', '891 - Fasilitas', 3000000, 400000, 1),
('TRY8907', '4138.007.054', '4138.007.054', '2019-09-12', '908 - Sumber Daya Manusia', 6700000, 1200000, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`kodekegiatan`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `pagu`
--
ALTER TABLE `pagu`
  ADD PRIMARY KEY (`akun`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `serapan`
--
ALTER TABLE `serapan`
  ADD PRIMARY KEY (`id_d_serapan`);

--
-- Indexes for table `serapan_h`
--
ALTER TABLE `serapan_h`
  ADD PRIMARY KEY (`id_h_serapan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `serapan`
--
ALTER TABLE `serapan`
  MODIFY `id_d_serapan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
