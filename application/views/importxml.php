<?php
require 'database.php';
if(isset($_POST['buttonImport'])) {
	copy($_FILES['xmlFile']['tmp_name'],
		'data'.$_FILES['xmlFile']['kode']);
	$pagu = simplexml_load_file('data'.$_FILES['xmlFile']['kode']);
	foreach($pagu as $pagu){
		$stmt = $database->prepare('insert into
			pagu(kode, nama_program, akun, jml_pagu)
			values(:kode, :nama_program, :akun, :jml_pagu)');
		$stmt->bindValue('kode', $pagu->kode);
		$stmt->bindValue('nama_program', $pagu->nama_program);
		$stmt->bindValue('akun', $pagu->akun);
		$stmt->bindValue('jml_pagu', $pagu->jml_pagu);
		$stmt->execute();
	}
}
$stmt = $database->prepare('select * from pagu');
$stmt->execute();
?>

<form method="post" enctype="multipart/form-data">
	XML File <input type="file" name="xmlFile">
	<br>
	<input type="submit" value="Import" name="buttonImport">
</form>
<br>
<h3>Product List</h3>
<table cellpadding="2" cellspacing="2" border="1">
	<tr>
		<th>kode</th>
		<th>nama program</th>
		<th>akun</th>
		<th>jml pagu</th>
	</tr>
	<?php while($product = $stmt->fetch(PDO::FETCH_OBJ)) { ?>
	<tr>
		<td><?php echo $product->kode; ?></td>
		<td><?php echo $product->nama_program; ?></td>
		<td><?php echo $product->akun; ?></td>
		<td><?php echo $product->jml_pagu; ?></td>
	</tr>
	<?php } ?>
</table>