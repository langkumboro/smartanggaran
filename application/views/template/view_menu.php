<section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url();?>assets/admin-lte/dist/img/avatar2.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>REMON</p>
          <a href="#"><i class="fa fa-circle text-success"></i>Ready</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <!-- <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div> -->
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?=base_url();?>dashboard/index"><i class="fa fa-circle-o"></i>Dashboard</a></li>
          </ul>
        </li>
      <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>DATA MASTER</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url();?>pagu/listpagu"><i class="fa fa-circle-o"></i>Daftar Pagu</a></li>
            <!--  <li><a href="<?=base_url();?>program/listprogram"><i class="fa fa-circle-o"></i>Daftar Program</a></li> -->
          </ul>
        </li>

        

        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i>
            <span>LAPORAN</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url();?>pembelian/listpembelian"><i class="fa fa-circle-o text-red"></i>View Laporan Detil Serapan</a></li>
            <li><a href="<?=base_url();?>penjualan/listpenjualan"><i class="fa fa-circle-o text-blue"></i>Laporan Serapan</a></li>
            
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i>
            <span>CREATE</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="<?=base_url();?>pembelian/dtp"><i class="fa fa-circle-o text-red"></i>Create Serapan</a></li>
         
            
          </ul>
        </li>
      </ul>

      <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <tr bgcolor="white"><center><b><font color=Black>@Copyright MONIT 2019</color></center> </b></tr>
     </section>
  
