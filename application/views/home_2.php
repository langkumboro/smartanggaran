<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Smart Anggaran</title>

        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet"  href="<?=base_url();?>assets/ace/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?=base_url();?>assets/ace/font-awesome/4.5.0/css/font-awesome.min.css" />

        <!-- page specific plugin styles -->

        <!-- text fonts -->
        <link rel="stylesheet" href="<?=base_url();?>assets/ace/css/fonts.googleapis.com.css" />

        <!-- ace styles -->
        <link rel="stylesheet" href="<?=base_url();?>assets/ace/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
            <link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="<?=base_url();?>assets/ace/css/ace-skins.min.css" />
        <link rel="stylesheet" href="<?=base_url();?>assets/ace/css/ace-rtl.min.css" />

        <!--[if lte IE 9]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <script src="<?=base_url();?>assets/ace/js/ace-extra.min.js"></script>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <?php $this->load->view('layout/menu_atas'); ?>
<body class="no-skin">
        
       <?php $this->load->view('layout/menu_samping'); ?>
</span>
    </a>
     </li>
        </ul>
        <?php $this->load->view($content); ?>
        </div>

            <?php $this->load->view('layout/footer'); ?>

<script>
  <script>
    $(function () {
        $(".date").datepicker();
    });
</script>
</script>
        <!--[if !IE]> -->
        <script src="<?=base_url();?>assets/ace/js/jquery-2.1.4.min.js"></script>

        <!-- <![endif]-->

        <!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
        <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src=<?=base_url();?>assets/ace/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>
        <script src="<?=base_url();?>assets/ace/js/bootstrap.min.js"></script>

       <!-- page specific plugin styles -->
        <link rel="stylesheet" href="<?=base_url();?>assets/ace/css/jquery-ui.min.css" />
        <link rel="stylesheet" href="<?=base_url();?>assets/ace/css/bootstrap-datepicker3.min.css" />
        <link rel="stylesheet" href="<?=base_url();?>assets/ace/css/ui.jqgrid.min.css" />

        <!--[if lte IE 8]>
          <script src="assets/js/excanvas.min.js"></script>
        <![endif]-->
        <script src="<?=base_url();?>assets/ace/js/jquery-ui.custom.min.js"></script>
        <script src="<?=base_url();?>assets/ace/js/jquery.ui.touch-punch.min.js"></script>
        <script src="<?=base_url();?>assets/ace/js/jquery.easypiechart.min.js"></script>
        <script src="<?=base_url();?>assets/ace/js/jquery.sparkline.index.min.js"></script>
        <script src="<?=base_url();?>assets/ace/js/jquery.flot.min.js"></script>
        <script src="<?=base_url();?>assets/ace/js/jquery.flot.pie.min.js"></script>
        <script src="<?=base_url();?>assets/ace/js/jquery.flot.resize.min.js"></script>

        <!-- ace scripts -->
        <script src="<?=base_url();?>assets/ace/js/ace-elements.min.js"></script>
        <script src="<?=base_url();?>assets/ace/js/ace.min.js"></script>

       
</script>      
    </body>
</html>