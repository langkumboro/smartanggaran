 <script>
    $( function() {
        $( "#tgl_awal" ).datepicker({dateFormat: "dd-mm-yy"});
        $( "#tgl_akhir" ).datepicker({dateFormat: "dd-mm-yy"});
    } );
  </script>
<form method="POST" action="<?=base_url();?>serapanH/laporan">
<h3 class="box-title"><font color="blue"><b>REPORT</b></font> SERAPAN HEADER</h3>

<div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Tanggal Awal Bulan</label>
                 <input type="text" id='tgl_awal' class="form-control"
                autocomplete="off" placeholder="Tentukan Tanggal Awal Bulan"
                onClick="(this.type='date')"  name="tgl_awal" 
                Placeholder ="Tentukan Tanggal Awal Bulan" >
                </div>

<div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Tanggal Akhir Bulan</label>
                 <input type="text" id='tgl_akhir' class="form-control"
                autocomplete="off" placeholder="Tentukan Tanggal Akhir Bulan"
                onClick="(this.type='date')"  name="tgl_akhir" 
                Placeholder ="Tentukan Tanggal Akhir Bulan" >
                </div>
<button class="btn btn-primary" type="submit"><i class="fa fa-spinner "></i> Proses</button>
<button class="btn btn-default" type="button" onclick="self.history.back()"><i class="fa fa-undo"></i> Back</button>
 <script>
   $(document).ready(function(){

    $('#proses').on('click', function(event){
      event.preventDefault();
      var tgl_awal  = $('#tgl_awal').val();
      var tgl_akhir = $('#tgl_akhir').val();

      if (tgl_awal == '' || tgl_akhir == '') {
        alert('Tanggal Tidak Boleh Kosong');
      }else if (new Date(tgl_awal) > new Date(tgl_akhir)) . {
        alert('Format Waktu Salah Input');
      }else{
        $('#forms').submit();
      }
    });
   });
 </script>