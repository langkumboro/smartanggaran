<?php
foreach ($edit_pagu as $data) {
  $kode                    = $data->kode.'-'.$data->nama_program;
  $akun                    = $data->akun;
  $jml_pagu                = $data->jml_pagu;
 
}
 
?>
<div style="color: red" align="center"><?= validation_errors(); ?></div>
<form action="<?=base_url()?>pagu/editpagu/<?=$akun;?>" method="POST"
 enctype="multipart/form-data">
  
            <div class="box-header with-border">
              <h3 class="box-title"><font color="blue"><b>EDIT</b></font> PAGU</h3>
            </div>
            
           
              <div class="box-body">
                
                <div class="form-group" style="width: 55% ">
                  <label for="exampleInputPassword1">Kode Pagu</label>
                  <input value="<?= $kode; ?>" type="text" class="form-control" name="kode" id="kode" maxlength="200" autocomplete="off" readonly>
                  
                </div>

                <!-- <div class="form-group" style="width: 55% ">
                  <label for="exampleInputEmail1">Nama Pagu</label>
                 <input value="<?= $kode; ?>" type="text" class="form-control" name="kode" id="kode"  maxlength="200" >
                </div> -->

                 <div class="form-group" style="width: 55% ">
                  <label for="exampleInputEmail1">Kode Akun</label>
                 <input value="<?= $akun; ?>" type="text" class="form-control" name="akun" id="akun"  maxlength="200" >
                </div>

                 <div class="form-group" style="width: 55% ">
                  <label for="exampleInputEmail1">Jumlah Pagu</label>
                 <input value="<?= $jml_pagu; ?>" type="text" class="form-control" name="jml_pagu" id="jml_pagu"  maxlength="200" >
                </div>

                
              <!-- /.box-body -->

            <!-- untuk kondisi save,riset, back -->
            <div class="box-footer">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i>  Save</button>                           
                <button class="btn btn-default" type="button" onclick="self.history.back()"><i class="fa fa-undo"></i> Back</button>
            </div>
            </form>
         