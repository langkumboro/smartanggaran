<body>
  <div style="color: red" align="center"><?= validation_errors(); ?></div>
  <form action="<?=base_url()?>serapanD/inputSerapanD" method="POST" 
    enctype="multipart/form-data">
   
            <div class="box-header with-border">
                <h3 class="box-title"><font color="blue"><b>INPUT</b></font> LAPORAN SERAPAN</h3>
            </div>
            

              <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Kode Kegiatan</label>
                    <select  id="kode" class="form-control"  name="kodekegiatan" value="<?= set_value('kodekegiatan');?>">
                        <?php foreach ($data_kegiatan as $data) { ?>
                          <option value="<?=$data->kodekegiatan; ?>">
                          <?= $data->kodekegiatan;?> -
                          <?= $data->judulkegiatan; ?>                          
                           </option>
                          <?php } ?>
                    </select>
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Jenis Perwab</label>
                      <select name="jenis_perwab"  class="form-control" id="jenis_perwab" value="<?= set_value('jenis_perwab');?>" />>
                          <option value="LS">LS</option>
                          <option value="GU">GU</option>
                      </select>
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Nilai Kwitansi</label>
                  <input type="text" class="form-control" name="nilai_kwit" id="nilai_kwit" value="<?=set_value('nilai_kwit');?>"
                  maxlength="200" autocomplete="off" placeholder="Kwitansi">
                </div>

                 <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">NO SPP</label>
                  <input type="text" class="form-control" name="nospp" id="nospp" value="<?=set_value('nospp');?>"
                  maxlength="200" autocomplete="off" placeholder="isi no spp">
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Tgl SPP</label>
                 <input type="text" id='tgl_spp' class="form-control"
                autocomplete="off" placeholder="Tentukan Tanggal"
                onClick="(this.type='date')"  name="tgl_spp" 
                Placeholder ="Tentukan Tanggal" value="<?=set_value('tgl_spp');?>">
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Kode Program</label>
                  
                    <select  id="kode" class="form-control"  name="kode" value="<?= set_value('kode');?>">
                        <?php foreach ($data_program as $data) { ?>
                          <option value="<?=$data->kode; ?>">
                          <?= $data->kode;?> -
                          <?= $data->nama_program; ?>                          
                           </option>
                          <?php } ?>
                    </select>
                </div>

               <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Akun</label>
                  
                    <select  id="akun" class="form-control"  name="akun" value="<?= set_value('akun');?>">
                        <?php foreach ($data_pagu as $data) { ?>
                          <option value="<?=$data->akun; ?>">
                          <?= $data->akun; ?>                          
                           </option>
                          <?php } ?>
                    </select>
                </div>
 

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Uraian</label>
                  <textarea name="uraian" class="form-control" id="uraian" cols="45" rows="3"><?=set_value('uraian');?></textarea>
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">PPN</label>
                  <input type="text" class="form-control" name="ppn" id="ppn" value="<?=set_value('ppn');?>"
                  maxlength="200" autocomplete="off" placeholder="ppn">
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">PPH21</label>
                  <input type="text" class="form-control" name="pph21" id="pph21" value="<?=set_value('pph21');?>"
                  maxlength="200" autocomplete="off" placeholder="PPH21">
                </div>
                
                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">PPH22</label>
                  <input type="text" class="form-control" name="pph22" id="pph22" value="<?=set_value('pph22');?>"
                  maxlength="200" autocomplete="off" placeholder="PPH22">
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">PPH23</label>
                  <input type="text" class="form-control" name="pph23" id="pph23" value="<?=set_value('pph23');?>"
                  maxlength="200" autocomplete="off" placeholder="PPH23">
                </div>
               
                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Status</label>
                  <select name="status"  class="form-control" id="status" value="<?= set_value('status');?>" />>
                          <option value="DRAFT">DRAFT</option>
                          <option value="DISETUJUI">DISETUJUI</option>
                          <option value="SEMUA">SEMUA</option>
                      </select>
                </div>



                


                

                </div>
              </div>
              <!-- /.box-body -->

             <!-- untuk kondisi save,riset, back -->
            <div class="box-footer">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i>  Save</button>                           
                <button class="btn btn-default" type="button" onclick="self.history.back()"><i class="fa fa-undo"></i> Back</button>
            </div>
            </form>
</body>
