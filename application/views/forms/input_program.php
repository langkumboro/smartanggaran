 <form action="<?=base_url()?>program/inputprogram" method="POST" 
    enctype="multipart/form-data">
   
            <div class="box-header with-border">
               <h3 class="box-title"><font color="blue"><b>INPUT</b></font> PROGRAM</h3>
            </div>
            
           
              
                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Kode Program</label>
                  <input type="text" class="form-control" name="kode" id="kode" value="<?=set_value('kode');?>" maxlength="200" autocomplete="off" placeholder="Kode Program">
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputEmail1">Nama Program</label>
                 <input type="text" class="form-control" name="nama_program" id="nama_program" value="<?=set_value('nama_program');?>" maxlength="200"  placeholder="Masukan Nama Program" autocomplete="off">
                </div>
              </div>
              <!-- /.box-body -->

             <!-- untuk kondisi save,riset, back -->
            <div class="box-footer">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i>  Save</button>                           
                <button class="btn btn-default" type="button" onclick="self.history.back()"><i class="fa fa-undo"></i> Back</button>
            </div>
            </form>


