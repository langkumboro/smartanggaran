<?php
foreach ($edit_kegiatan as $data) {
  $kodekegiatan      = $data->kodekegiatan;
  $tgl_pel           = $data->tgl_pel;
  $lokasi            = $data->lokasi;
  $judulkegiatan     = $data->judulkegiatan;
 
}
 
?>
<div style="color: red" align="center"><?= validation_errors(); ?></div>
<form action="<?=base_url()?>kegiatan/editkegiatan/<?=$kodekegiatan;?>" method="POST"
 enctype="multipart/form-data">
  
            <div class="box-header with-border">
              <h3 class="box-title"><font color="blue"><b>EDIT</b></font> KEGIATAN</h3>
            </div>
            
           
              <div class="box-body">
                
                <div class="form-group" style="width: 55% ">
                  <label for="exampleInputPassword1">Kode Kegiatan</label>
                  <input value="<?= $kodekegiatan; ?>" type="text" class="form-control" name="kodekegiatan" id="kodekegiatan" autocomplete="off" readonly>
                </div>

                 <div class="form-group" style="width: 55% ">
                  <label for="exampleInputPassword1">Tgl Pelaksanaan</label>
                 <input type="text" id='tgl_pel' class="form-control"
                autocomplete="off" placeholder="Tentukan Tanggal"
                onClick="(this.type='date')" name="tgl_pel" 
                value="<?= $tgl_pel; ?>">
                </div>

               
                

                 <div class="form-group" style="width: 55% ">
                  <label for="exampleInputEmail1">Lokasi Pelaksanaan</label>
                 <input value="<?= $lokasi; ?>" type="text" class="form-control" name="lokasi" id="lokasi"  maxlength="200" >
                </div>

                 <div class="form-group" style="width: 55% ">
                  <label for="exampleInputEmail1">Judul Kegiatan</label>
                 <input value="<?= $judulkegiatan; ?>" type="text" class="form-control" name="judulkegiatan" id="judulkegiatan"  maxlength="200" >
                </div>

             
              <div class="box-footer">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i>  Save</button>                           
                <button class="btn btn-default" type="button" onclick="self.history.back()"><i class="fa fa-undo"></i> Back</button>
              </div>
            </form>
         