<!-- css -->
<style type="text/css">
*, *:before, *:after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

body {
  background: #105469;
  font-family: 'Open Sans', sans-serif;
}
table {
  background: #012B39;
  border-radius: 0.25em;
  border-collapse: collapse;
  margin: 1em;
}

td {
  color: #fff;
  font-weight: 400;
  padding: 0.65em 1em;
}
.disabled td {
  color: #4F5F64;
}
tbody tr {
  transition: background 0.25s ease;
}
tbody tr:hover {
  background: #014055;
}
</style>

<!-- TABLE -->

<h3 class="box-title"><font color="blue"><b>TRANSAKSI</b></font> SERAPAN DETAIL</h3>
                            
<table border="1" width="100%">
   
      <?php
        if($this->session->flashdata('info') == true){
          echo $this->session->flashdata('info');
        }
        ?>
          <div class="btn-toolbar pull-right" style="margin-top:5px;">   
                <div class="btn-group">  
                 <!-- <form action="<?=base_url()?>serapanH/listserapanH" method="POST" -->
                 <form action="<?=base_url()?>serapanD/listserapanD" method="POST" 
                    class="sidebar-form">
                    <label class="btn-group">                       
                      <div class="input-group">
                        <input type="text" name="caridata" class="form-control" 
                              placeholder="Search..." autocomplete="off">
                      </div>                        
                    </label>
                    <label class="btn-group">
                      <button style="height:34px;" class="btn btn-default" title="Refresh" type="submit" name="tombol_cari" id="search-btn"><i class="fa fa-search"></i></button>
                    </label>
                 </form>  
                </div>                                 
          </div>
</td>
  <thead>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="<?=base_url();?>serapanD/inputSerapanD" style="text-decoration:none;"><i class="fa fa-user-plus fa-2x"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    
    <tr> 
      <td align="center" width="2%"><font color="gold"><b>No</font></td>
      <td align="center"><font color="gold"><b>Kode Program</font></td>
      <td align="center"><font color="gold"><b>No SPP</font></td>
      <td align="center"><font color="gold"><b>Tgl SPP</font></td>
      <td align="center"><font color="gold"><b>Uraian Pekerjaan</font></td>
      <td align="center"><font color="gold"><b>Nilai Kwitansi</font></td>
      <td align="center"><font color="gold"><b>PPN [Pajak]</font></td>  
     <!--  <td align="center"><font color="gold"><b>PPH21 [Pajak]</font></td>
      <td align="center"><font color="gold"><b>PPH23 [Pajak]</font></td>
      <td align="center"><font color="gold"><b>PPH22 [Pajak]</font></td>
      <td align="center"><font color="gold"><b>STATUS</font></td>
      <td align="center" bgcolor="black"><font color="gold"><b>Aksi</font></td> -->
    </tr>
      
  </thead>
   <?php
        $data_posisi = $this->uri->segment(4);
        $no = $data_posisi;
        if (count($data_serapanD) > 0) {
        foreach ($data_serapanD as $data) {
        $no++;
        
        ?>
                </thead>
                <tbody>
                <tr>
                  <td align="center"><?= $no; ?></td>
                  <td align="center"><a href=""><i class="fa fa-edit fa-lg"><b><?= $data->kode; ?></b></i></a></td>
                  <td align="center"><?= $data->nospp; ?></td>
                  <td align="center"><?= $data->tgl_spp; ?></td>
                  <td align="center"><?= $data->uraian_pek; ?></td>
                  <td align="center">Rp.<?= number_format($data->nilai_kwit); ?></td>
                  <td align="center"><?= $data->ppn; ?></td>
                 <!--  <td align="center">Rp. <?= number_format($data->pph21); ?></td>
                  <td align="center">Rp. <?= number_format($data->pph22); ?> </td>
                  <td align="center">Rp. <?= number_format($data->pph23);?> </td>
                  <td align="center"><?= $data->status; ?></td> -->
                  <!-- <td align="center" bgcolor="black">  
                      
                      <a href="<?=base_url();?>serapanD/deleteserapanD/<?= $data->id_d_serapan; ?>" 
                      onclick="return confirm('Are You Sure Delete Data?');" style="text-decoration:none;">
                       <font color="red"><i class="fa fa-trash-o fa-2x"></i></font></a>
                  </td> -->
                  

                 
          </tr>
              <?php } ?>
  </table>
  </tbody>
</table>
<!-- <b><?php echo $totalserapanD; ?> dari <?php echo $totalserapan; ?> data transaksi</b>
    <center> <tr>
          <td align="center"><?= $this->pagination->create_links();?></td>
    </tr>
    <?php } else {
     ?>
         <tr>
         <td colspan="7" align="center">--Tidak Ada Datanya--</td>
         </tr>
   <?php } ?>
                </td>
                </tr>
    </center>
                
                </tbody>
                
              </table>
            </div>