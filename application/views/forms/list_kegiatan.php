<?php
  $conn = new PDO("mysql:host=localhost;dbname=monitoring", 'root', '');
?>

<?php
if(isset($_POST['buttonImport'])) {
  copy($_FILES['xmlFile']['tmp_name'],
    'data'.$_FILES['xmlFile']['name']);
  $data_kegiatan = simplexml_load_file('data'.$_FILES['xmlFile']['name']);
  foreach($data_kegiatan as $data){
    $stmt = $conn->prepare('insert into
      kegiatan(kodekegiatan, tgl_pel, lokasi, judulkegiatan)
      values(:kodekegiatan, :tgl_pel, :lokasi, :judulkegiatan)');
    $stmt->bindValue('kodekegiatan', $data->kodekegiatan);
    $stmt->bindValue('tgl_pel', $data->tgl_pel);
    $stmt->bindValue('lokasi', $data->lokasi);
    $stmt->bindValue('judulkegiatan', $data->judulkegiatan);
    $stmt->execute();
  }
}
$stmt = $conn->prepare('select * from kegiatan');
$stmt->execute();
?>

<!-- css -->
<style type="text/css">
*, *:before, *:after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

body {
  background: #105469;
  font-family: 'Open Sans', sans-serif;
}
table {
  background: #012B39;
  border-radius: 0.25em;
  border-collapse: collapse;
  margin: 1em;
}

td {
  color: #fff;
  font-weight: 400;
  padding: 0.65em 1em;
}
.disabled td {
  color: #4F5F64;
}
tbody tr {
  transition: background 0.25s ease;
}
tbody tr:hover {
  background: #014055;
}
</style>

<!-- TABLE -->

<h3 class="box-title"><font color="blue"><b>DAFTAR</b></font> KEGIATAN</h3>
                            
<table border="1" width="100%">
  <form method="post" enctype="multipart/form-data">
  XML File <input type="file" name="xmlFile">
  <br>
  <input type="submit" value="Import" name="buttonImport">
</form>    
      <?php
        if($this->session->flashdata('info') == true){
          echo $this->session->flashdata('info');
        }
        ?>
          <div class="btn-toolbar pull-right" style="margin-top:5px;">   
                <div class="btn-group">  
                 <form action="<?=base_url()?>kegiatan/listkegiatan" method="POST" 
                    class="sidebar-form">
                    <label class="btn-group">                       
                      <div class="input-group">
                        <input type="text" name="caridata" class="form-control" 
                              placeholder="Search..." autocomplete="off">
                      </div>                        
                    </label>
                    <label class="btn-group">
                      <button style="height:34px;" class="btn btn-default" title="Refresh" type="submit" name="tombol_cari" id="search-btn"><i class="fa fa-search"></i></button>
                    </label>
                 </form>  
                </div>                                 
          </div>
</td>
  <thead>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="<?=base_url();?>kegiatan/inputkegiatan" style="text-decoration:none;"><i class="fa fa-user-plus fa-2x"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="<?=base_url();?>kegiatan/exportxml"style="text-decoration:none;"><i class="fa fa-upload fa-2x"></i></a>
    <tr> 
      <td align="center" width="2%"><font color="gold"><b>No</font></td>
      <td align="center"><font color="gold"><b>Kode Kegiatan</font></td>
      <td align="center"><font color="gold"><b>Tgl Pelaksanaan</font></td>
      <td align="center"><font color="gold"><b>Lokasi</font></td>
      <td align="center"><font color="gold"><b>Judul Kegiatan</font></td>
      <td align="center" bgcolor="black"><font color="gold"><b>Aksi</font></td>

  </thead>
   <?php
        $data_posisi = $this->uri->segment(4);
        $no = $data_posisi;
        if (count($data_kegiatan) > 0) {
        foreach ($data_kegiatan as $data) {
        $no++;
        
        ?>
                </thead>
                <tbody>
                <tr>
                  <td align="center"><?= $no; ?></td>
                  <td align="center"><a href="<?=base_url();?>kegiatan/editkegiatan/<?= $data->kodekegiatan; ?>"><i class="fa fa-edit fa-lg"><b><?= $data->kodekegiatan; ?></b></i></a></td>
                  <td align="center"><?= $data->tgl_pel; ?></td>
                  <td align="center"><?= $data->lokasi; ?></td>
                  <td align="center"><?= $data->judulkegiatan; ?></td>
                  <td align="center" bgcolor="black">  
                      
                      <a href="<?=base_url();?>kegiatan/deletekegiatan/<?= $data->kodekegiatan; ?>" 
                      onclick="return confirm('Are You Sure Delete Data?');" style="text-decoration:none;">
                       <font color="red"><i class="fa fa-trash-o fa-2x"></i></font></a>
                  </td>
          </tr>
              <?php } ?>
  </table>
  </tbody>
</table>
<b><?php echo $totalkegiatan; ?> dari <?php echo $totalkegiatan; ?> data</b>

    <center> <tr>
          <td align="center"><?= $this->pagination->create_links();?></td>
    </tr>
    <?php } else {
     ?>
         <tr>
         <td colspan="7" align="center">--Tidak Ada Datanya--</td>
         </tr>
   <?php } ?>
                </td>
                </tr>
    </center>
                
                </tbody>
                
              </table>
            </div>
