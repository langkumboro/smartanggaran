<body>
  <div style="color: red" align="center"><?= validation_errors(); ?></div>
  <form action="<?=base_url()?>serapanH/inputSerapan" method="POST" 
    enctype="multipart/form-data">
   
            <div class="box-header with-border">
                <h3 class="box-title"><font color="blue"><b>INPUT</b></font> LAPORAN SERAPAN</h3>
            </div>
            

              <div class="box-body" style="center">
                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">ID Transaksi</label>
                  <input type="text" class="form-control" name="id_h_serapan" id="id_h_serapan"
                  maxlength="200" autocomplete="off" placeholder="Kode Akun">
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Kode Program</label>
                  
                    <select  id="kode" class="form-control"  name="kode" value="<?= set_value('kode');?>">
                        <?php foreach ($data_program as $data) { ?>
                          <option value="<?=$data->kode; ?>">
                          <?= $data->kode;?> -
                          <?= $data->nama_program; ?>                          
                           </option>
                          <?php } ?>
                    </select>
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Nama Program</label>
                  
                    <select  id="nama_program" class="form-control"  name="nama_program" value="<?= set_value('nama_program');?>">
                        <?php foreach ($data_program as $data) { ?>
                          <option value="<?=$data->kode; ?>">
                          <?= $data->nama_program; ?>                          
                           </option>
                          <?php } ?>
                    </select>
                </div>
                

               
                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Jumlah Pagu [IDR]</label>
                  <select  id="jml_pagu" class="form-control"  name="akun" value="<?= set_value('jml_pagu');?>">
                        <?php foreach ($data_pagu as $data) { ?>
                          <option value="<?=$data->akun; ?>">
                          <?= $data->jml_pagu; ?>                          
                           </option>
                          <?php } ?>
                    </select>
                </div>



                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Realisasi</label>
                  <input type="text" class="form-control" name="realisasi" id="realisasi" value="<?=set_value('realisasi');?>"
                  maxlength="200" autocomplete="off" placeholder="Kode Akun">
                </div>


                

                </div>
              </div>
              <!-- /.box-body -->

             <!-- untuk kondisi save,riset, back -->
            <div class="box-footer">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i>  Save</button>                           
                <button class="btn btn-default" type="button" onclick="self.history.back()"><i class="fa fa-undo"></i> Back</button>
            </div>
            </form>
</body>
