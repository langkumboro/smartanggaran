<body>
  <div style="color: red" align="center"><?= validation_errors(); ?></div>
  <form action="<?=base_url()?>pagu/inputpagu" method="POST" 
    enctype="multipart/form-data">
   
            <div class="box-header with-border">
                <h3 class="box-title"><font color="blue"><b>INPUT</b></font> PAGU</h3>
            </div>
            

              <div class="box-body" style="center">
                
                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Kode Program</label>
                  
                    <select  id="kode" class="form-control"  name="kode" value="<?= set_value('kode');?>">
                        <?php foreach ($data_program as $data) { ?>
                          <option value="<?=$data->kode; ?>">
                          <?= $data->kode;?> -
                          <?= $data->nama_program; ?>                          
                           </option>
                          <?php } ?>
                    </select>
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Nama Program</label>
                  
                    <select  id="nama_program" class="form-control"  name="nama_program" value="<?= set_value('nama_program');?>">
                        <?php foreach ($data_program as $data) { ?>
                          <option value="<?=$data->nama_program; ?>">
                          <?= $data->nama_program; ?>                          
                           </option>
                          <?php } ?>
                    </select>
                </div>
                

                <!-- <td><label>Kode Program</label></td>
          <td>
            <select name="kode" onchange="changeValue(this.value)" >
                <option>- Pilih -</option>
                <?php if(mysqli_num_rows($program)) {?>
                    <?php while($row_pro= mysqli_fetch_array($program)) {?>
                        <option value="<?php echo $row_pro["kode"]?>"> <?php echo $row_brg["kode"]?> </option>
                    <?php $jsArray .= "nama_program['" . $row_brg['kode'] . "'] = {nama_program:'" . addslashes($row_pro['nama_program']) . "'};\n"; } ?>
                <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td><label>Nama Program</label></td>
          <td><input type="text" class="form-control" name="nama_program" id="nama_program" value="0" ></td>
        </tr>

 -->
                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Kode Akun</label>
                  <input type="text" class="form-control" name="akun" id="akun" value="<?=set_value('akun');?>"
                  maxlength="200" autocomplete="off" placeholder="Kode Akun">
                </div>


                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Jumlah Pagu</label>
                  <input type="text" class="form-control" name="jml_pagu" id="jml_pagu" value="<?=set_value('jml_pagu');?>"
                  maxlength="200"  autocomplete="off" placeholder="IDR">
                </div>

                </div>
              </div>
              <!-- /.box-body -->

             <!-- untuk kondisi save,riset, back -->
            <div class="box-footer">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i>  Save</button>                           
                <button class="btn btn-default" type="button" onclick="self.history.back()"><i class="fa fa-undo"></i> Back</button>
            </div>
            </form>
</body>
