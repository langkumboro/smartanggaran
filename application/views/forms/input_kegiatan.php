 <div style="color: red" align="center"><?= validation_errors(); ?></div>
 <form action="<?=base_url()?>kegiatan/inputkegiatan" method="POST" 
    enctype="multipart/form-data">
   
            <div class="box-header with-border">
              <h3 class="box-title"><font color="blue"><b>INPUT</b></font> KEGIATAN</h3><br>

            </div>

              <div class="box-body" style="center">
                 <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Kode Kegiatan</label>
                   <input type="text"  class="form-control"  name="kodekegiatan" id="kodekegiatan"  value="<?=$kode_baru?>" readonly/>
                </div>
                 <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Tgl Pelaksanaan</label>
                 <input type="text" id='tgl_pel' class="form-control"
                autocomplete="off" placeholder="Tentukan Tanggal"
                onClick="(this.type='date')"  name="tgl_pel" 
                Placeholder ="Tentukan Tanggal" value="<?=set_value('tgl_pel');?>">
                </div>
               

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Lokasi Pelaksanaan</label>
                  <input type="text" class="form-control" name="lokasi" id="lokasi" value="<?=set_value('lokasi');?>"
                  maxlength="200" autocomplete="off" placeholder="isi lokasi pelaksanaan">
                </div>


                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Judul Kegiatan</label>
                  <input type="text" class="form-control" name="judulkegiatan" id="judulkegiatan" value="<?=set_value('judulkegiatan');?>"
                  maxlength="200"  autocomplete="off" placeholder="isi Judul Kegiatan">
                </div>

                </div>
              </div>
              
              <!-- untuk kondisi save,riset, back -->
            <div class="box-footer">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i>  Save</button>                           
                <button class="btn btn-default" type="button" onclick="self.history.back()"><i class="fa fa-undo"></i> Back</button>
            </div>
            </form>