<!-- css -->
<style type="text/css">
*, *:before, *:after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

body {
  background: #105469;
  font-family: 'Open Sans', sans-serif;
}
table {
  background: #012B39;
  border-radius: 0.25em;
  border-collapse: collapse;
  margin: 1em;
}

td {
  color: #fff;
  font-weight: 400;
  padding: 0.65em 1em;
}
.disabled td {
  color: #4F5F64;
}
tbody tr {
  transition: background 0.25s ease;
}
tbody tr:hover {
  background: #014055;
}
</style>

<!-- TABLE -->

<h3 class="box-title"><font color="blue"><b>TRANSAKSI</b></font> SERAPAN</h3>
                            
<table border="1" width="100%">
   
      <?php
        if($this->session->flashdata('info') == true){
          echo $this->session->flashdata('info');
        }
        ?>
          <div class="btn-toolbar pull-right" style="margin-top:5px;">   
                <div class="btn-group">  
                 <!-- <form action="<?=base_url()?>serapanH/listserapanH" method="POST" -->
                 <form action="<?=base_url()?>serapanH/listserapanH" method="POST" 
                    class="sidebar-form">
                    <label class="btn-group">                       
                      <div class="input-group">
                        <input type="text" name="caridata" class="form-control" 
                              placeholder="Search..." autocomplete="off">
                      </div>                        
                    </label>
                    <label class="btn-group">
                      <button style="height:34px;" class="btn btn-default" title="Refresh" type="submit" name="tombol_cari" id="search-btn"><i class="fa fa-search"></i></button>
                    </label>
                 </form>  
                </div>                                 
          </div>
</td>
  <thead>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="<?=base_url();?>serapanH/inputSerapan" style="text-decoration:none;"><i class="fa fa-user-plus fa-2x"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    
    <tr> 
      <td align="center" width="2%"><font color="gold"><b>No</font></td>
      <td align="center"><font color="gold"><b>Kode Program</font></td>
      <td align="center"><font color="gold"><b>Akun</font></td>
      <td align="center"><font color="gold"><b>Pagu [IDR]</font></td>
      <td align="center"><font color="gold"><b>Realisasi</font></td>
      <td align="center"><font color="gold"><b>Dana Sisa</font></td>
      <td align="center" bgcolor="black"><font color="gold"><b>Aksi</font></td>
      
  </thead>
   <?php
        $data_posisi = $this->uri->segment(4);
        $no = $data_posisi;
        if (count($data_serapanH) > 0) {
        foreach ($data_serapanH as $data) {
        $no++;
        
        ?>
                </thead>
                <tbody>
                <tr>
                  <td align="center"><?= $no; ?></td>
                  <td align="center"><?= $data->kode. ' -  '.$data->nama_program; ?></td>
                  <td align="center"><?= $data->akun; ?></td>
                  <td align="center">Rp. <?= number_format($data->jml_pagu); ?> </td>
                  <td align="center">Rp. <?= number_format($data->realisasi);?> </td>
                  <td align="center">Rp. <?= number_format($data->sisa_dana);?> </td>
                  <td align="center" bgcolor="black">  
                      
                      <a href="<?=base_url();?>serapanH/deleteserapan/<?= $data->id_h_serapan; ?>" 
                      onclick="return confirm('Are You Sure Delete Data?');" style="text-decoration:none;">
                       <font color="red"><i class="fa fa-trash-o fa-2x"></i></font></a>
                  </td>
                  

                 
          </tr>
              <?php } ?>
  </table>
  </tbody>
</table>
<b><?php echo $totalserapan; ?> dari <?php echo $totalserapan; ?> data transaksi</b>
    <center> <tr>
          <td align="center"><?= $this->pagination->create_links();?></td>
    </tr>
    <?php } else {
     ?>
         <tr>
         <td colspan="7" align="center">--Tidak Ada Datanya--</td>
         </tr>
   <?php } ?>
                </td>
                </tr>
    </center>
                
                </tbody>
                
              </table>
            </div>
