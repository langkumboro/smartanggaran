<!-- import json -->
<?php
  $conn = new PDO("mysql:host=localhost;dbname=monitoring", 'root', '');
?>

<?php
if(isset($_POST['buttomImport'])) {
    copy($_FILES['jsonFile']['tmp_name'],'data'.$_FILES['jsonFile']['name']);
  $data = file_get_contents('data'.$_FILES['jsonFile']['name']);
  $data_pagu = json_decode($data);
    foreach ($data_pagu as $pagu) {
        $stmt = $conn->prepare('insert into pagu(akun, kode, nama_program,jml_pagu) values(:akun, :kode, :nama_program, :jml_pagu)');
        $stmt->bindValue('akun', $pagu->akun);
        $stmt->bindValue('kode', $pagu->kode);
        $stmt->bindValue('nama_program', $pagu->nama_program);
        $stmt->bindValue('jml_pagu', $pagu->jml_pagu);
    $stmt->execute();
  }
}
?>




<!-- css -->
<style type="text/css">
*, *:before, *:after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

body {
  background: #105469;
  font-family: 'Open Sans', sans-serif;
}
table {
  background: #012B39;
  border-radius: 0.25em;
  border-collapse: collapse;
  margin: 1em;
}

td {
  color: #fff;
  font-weight: 400;
  padding: 0.65em 1em;
}
.disabled td {
  color: #4F5F64;
}
tbody tr {
  transition: background 0.25s ease;
}
tbody tr:hover {
  background: #014055;
}
</style>

<!-- TABLE -->


 <h3 class="box-title"><font color="blue"><b>DAFTAR</b></font> PAGU</h3>
                            
 <table cellpadding="5px" width="20%" align="center">
  <tr>
   <form method="post" enctype="multipart/form-data">
      JSON File <input type="file" name="jsonFile">
      <br>
      <input type="submit" value="Import" name="buttomImport">
    </form>
  </tr>
</table><br>

   <table border="1" width="100%">
      <?php
        if($this->session->flashdata('info') == true){
          echo $this->session->flashdata('info');
        }
        ?>
          <div class="btn-toolbar pull-right" style="margin-top:5px;">   
                <div class="btn-group">  
                 <form action="<?=base_url()?>pagu/listpagu" method="POST" 
                    class="sidebar-form">
                    <label class="btn-group">                       
                      <div class="input-group">
                        <input type="text" name="caridata" class="form-control" 
                              placeholder="Search..." autocomplete="off">
                      </div>                        
                    </label>
                    <label class="btn-group">
                      <button style="height:34px;" class="btn btn-default" title="Refresh" type="submit" name="tombol_cari" id="search-btn"><i class="fa fa-search"></i></button>
                    </label>
                 </form>  
                </div>                                 
          </div>
</td>


<!-- export json -->
  <?php
  if(isset($_POST['create'])){
    // $cn = new mysqli("localhost","root","","json");
    $query = $conn->query("SELECT * FROM pagu");

    $data = array();
    // $data["pagu"] =  array();
    foreach ($query as $rk) {
      $arrp = (array)$rk;

      $arr2['akun'] = $arrp['akun'];
      $arr2['kode'] = $arrp['kode'];
      $arr2['nama_program'] = $arrp['nama_program'];
      $arr2['jml_pagu'] = $arrp['jml_pagu'];
     

      // array_push($data["pagu"], $arr2);
       array_push($data, $arr2);
    }

    $json = json_encode($data,JSON_PRETTY_PRINT);

    $fileName = $_POST['fileName'].".json";

    $handle = fopen($fileName, 'w');

    $write = fwrite($handle, $json);

    if($write){
      $message = "Create ".$fileName." Is Successful.";
      fclose($handle);
    }
  }
?>
<h2>Creat Json File From Database</h2>
  <form method="post">
    <input type="text" name="fileName" placeholder="File Name" required="">
    <input type="submit" name="create" value="Create Json">
  </form>




  <thead>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="<?=base_url();?>pagu/inputpagu" style="text-decoration:none;"><i class="fa fa-user-plus fa-2x"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    
    <tr> 
      <td align="center" width="2%"><font color="gold"><b>No</font></td>
      <td align="center"><font color="gold"><b>Kode Program</font></td>
      <td align="center"><font color="gold"><b>Nama Program</font></td>
      <td align="center"><font color="gold"><b>Akun Pagu</font></td>
      <td align="center"><font color="gold"><b>Jumlah Pagu</font></td>
      <td align="center" bgcolor="black"><font color="gold"><b>Aksi</font></td>

  </thead>
   <?php
        $data_posisi = $this->uri->segment(4);
        $no = $data_posisi;
        if (count($data_pagu) > 0) {
        foreach ($data_pagu as $data) {
        $no++;
        
        ?>
                </thead>
                <tbody>
                <tr>
                  <td align="center"><?= $no; ?></td>
                  <td align="center"><a href="<?=base_url();?>pagu/editpagu/<?= $data->akun; ?>"><i class="fa fa-edit fa-lg"><b><?= $data->kode; ?></b></i></a></td>
                   <td align="center"><?=$data->nama_program; ?></td>
                  <td align="center"><?= $data->akun; ?></td>
                 <!--  <td align="center"><?=$data->jml_pagu;?></td> -->
                  <td align="center">Rp. <?= number_format($data->jml_pagu);?>,-</td>
                  <td align="center" bgcolor="black">  
                      
                      <a href="<?=base_url();?>pagu/deletepagu/<?= $data->akun; ?>" 
                      onclick="return confirm('Are You Sure Delete Data?');" style="text-decoration:none;">
                       <font color="red"><i class="fa fa-trash-o fa-2x"></i></font></a>
                  </td>
          </tr>
              <?php } ?>
  </table>
  </tbody>
</table>
<b><?php echo $totalpagu; ?> dari <?php echo $totalpagu; ?> data</b>

    <center> <tr>
          <td align="center"><?= $this->pagination->create_links();?></td>
    </tr>
    <?php } else {
     ?>
         <tr>
         <td colspan="7" align="center">--Tidak Ada Datanya--</td>
         </tr>
   <?php } ?>
                </td>
                </tr>
    </center>
                
                </tbody>
                
              </table>
            </div>
