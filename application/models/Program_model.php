<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Program_model extends CI_Model
{
	//panggil nama table
	private $_table = "program";
	
public function rulesinput()
	
	{
		return [
		[
		'field' => 'kode',
		'label' => 'Kode',
		'rules' => 'trim|required|max_length[100]|is_unique[pagu.kode]',
		'errors' => [
		   'required' => 'Kode tidak boleh Kosong.',
		   'max_length' => 'Kode tidak boleh lebih dari 100 karakter.',
		   'is_unique'	=> 'Kode tidak boleh sama',
		   ],
		   ],
		   [
		 'field' => 'nama_program',
		 'label' => 'Nama Program',
		 'rules' => 'required',
		 'errors' => [
		   'required' => 'Nama Perogram tidak boleh kosong.',

		   
						]
					],
					
		];
	}
	

public function rulesedit()
	
	{
		return [
		[
		'field' => 'kode',
		'label' => 'Kode',
		'rules' => 'trim|required|max_length[100]',
		'errors' => [
		   'required' => 'Kode tidak boleh Kosong.',
		   'max_length' => 'Kode tidak boleh lebih dari 100 karakter.',
		   ],
		   ],
		   [
		 'field' => 'nama_program',
		 'label' => 'Nama Program',
		 'rules' => 'required',
		 'errors' => [
		   'required' => 'Nama Perogram tidak boleh kosong.',
		
						]
					],
					
		];
		
	}
	
public function tampilDataProgram()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
public function tampilDataProgram2()
	
	{
		$query = $this->db->query("SELECT * FROM program");
		return $query->result();
	}
	
public function tampilDataProgram3()
	
	{
		$this->db->select('*');
		$this->db->order_by('kode', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}

public function save()

	{
		$kode_program = $this->input->POST('kode');
		$sql = $this->db->query("SELECT kode from program where kode = '$kode_program'");
		$cek_kode = $sql->num_rows();
		if ($cek_kode > 0)
		{
			$this->session->set_flashdata('kode');
			redirect('program/inputprogram');
		}else{
			$kode_program = $this->input->post('kode');
		}
		
		$data['kode'] =$kode_program;
		$data['nama_program'] =$this->input->post('nama_program');
		
		$this->db->insert($this->_table, $data);
	}


public function detailprogram($kode)
	{
		$this->db->select('*');
		$this->db->where('kode', $kode);
		$result = $this->db->get($this->_table);
		return $result->result();
	}

public function update($kode)
	{
		
		$data['kode']			= $this->input->post('kode');
		$data['nama_program']			= $this->input->post('nama_program');	
		
		$this->db->where('kode', $kode);
		$this->db->update($this->_table, $data);
	}
public function delete($kode)
	
	{
		
		$this->db->where('kode', $kode);
	    $this->db->delete($this->_table);
		
	}

public function hitungjmlprogram()
{   
    $query = $this->db->get('program');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
}
public function tampilDataProgramPagination($perpage, $uri, $data_pencarian)
	{
		//cek eror
		// echo "<pre>";
		// print_r($data_pencarian); die();
		// echo "</pre>";
		$this->db->select('*');
		if (!empty($data_pencarian)) {
			$this->db->like('nama_program', $data_pencarian);
		}
		$this->db->order_by('kode','asc');

		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return$get_data->result();
		}else{
			return null;
		}

	}
public function tombolpagination($data_pencarian)
{
	$this->db->like('nama_program', $data_pencarian);
	$this->db->from($this->_table);
	$hasil = $this->db->count_all_results();
	
	//pagination limt
	$pagination['base_url'] = base_url().'program/listprogram/load/';
	$pagination['total_rows'] =$hasil;
	$pagination['per_page'] = "3";
	$pagination['uri_segment'] = 4;
	$pagination['num_links'] = 2;
	
	
	$pagination['full_tag_open'] = '<div class="paging text-center"><nav><ul class="pagination justify-content-center">';
		$pagination['full_tag_close'] = '</ul></nav></div>';
		
		$pagination['first_link'] = 'First';
		$pagination['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['first_tagl_close'] = '</span></li>';
		
		$pagination['last_link'] = 'Last';
		$pagination['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['last_tagl_close'] = '</span></li>';
		
		$pagination['next_link'] = 'Next';
		$pagination['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
		
		$pagination['prev_link'] = 'Prev';
		$pagination['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['prev_tagl_close'] = '</span></li>';
		
		$pagination['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$pagination['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
		
		$pagination['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['num_tag_close'] = '</span></li>';
	
	$this->pagination->initialize($pagination);
	
	$hasil_pagination = $this->tampilDataProgramPagination($pagination['per_page'],
	$this->uri->segment(4), $data_pencarian);
	
	return $hasil_pagination;
	
	}


}
