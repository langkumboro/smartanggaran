<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class SerapanD_model extends CI_Model
{
	//panggil nama table
	private $_table = "serapan";
	
	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("Pagu_model");
		$this->load->model("Kegiatan_model");
    }

	
public function rulesinput()
	
	{
		return [
		[
		
		 'field' => 'kode',
		 'label' => 'Jenis',
		 'rules' => 'required',
		 'errors' => [
		   'required' => 'Jenis tidak boleh kosong.',
           
       
           ]
		   ]
		  		 ];
	}


	
	public function tampilDataSerapanD()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	// public function tampilDataSerapanD2()
	
	//  {
 //        $query	= $this->db->query(
 //            "SELECT A.*, C.jml_pagu, B.kode, B.nama_program FROM " . $this->_table . " 
 //            	  AS A 
 //            	  INNER JOIN pagu AS C ON A.akun = C.akun
 //            	  INNER JOIN program AS B ON C.kode = B.kode
            	 
 //             WHERE A.flag = 1");
        
 //        return $query->result();	
 //    }

	
	public function tampilDataSerapanD3()
	
	{
		$this->db->select('*');
		$this->db->order_by('id_d_serapan', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}

	
public function saveSerapanD()
    {
    	// echo "<pre>";
     //    print_r($this->Tampiljmlpagu); die();
     //    echo "</pre>";

        $id = $this->input->POST('id_d_serapan');
		$sql = $this->db->query("SELECT id_d_serapan from serapan where id_d_serapan = '$id'");
		$cek_id = $sql->num_rows();
		if ($cek_id > 0)
		{
			$this->session->set_flashdata('id_d_serapan');
			redirect('serapanD/listserapanD');
		}else{
			$id = $this->input->post('id_d_serapan');
		}
	
     //    echo "</pre>";
/*var_dump($variabel); die();*/
		$data['id_d_serapan'] =$id;
        $data['kode']    = $this->input->post('kode');
        $data['kodekegiatan']    = $this->input->post('kodekegiatan');
        $data['nilai_kwit']    = $this->input->post('nilai_kwit');
        $data['akun']    = $this->input->post('akun');
        $data['nospp']    = $this->input->post('nospp');
        $data['tgl_spp']        = $this->input->post('tgl_spp');
        $data['jenis_perwab']    = $this->input->post('jenis_perwab');
        $data['uraian_pek']    = $this->input->post('uraian_pek');
        $data['ppn']    = $this->input->post('ppn');
        $data['pph21']    = $this->input->post('pph21');
        $data['pph22']    = $this->input->post('pph22');
		$data['pph23']    = $this->input->post('pph23');
		$data['status']    = $this->input->post('status');       
        
			

		$this->db->insert($this->_table, $data);
    }


public function delete($id_d_serapan)
	
	{
		
		$this->db->where('id_d_serapan', $id_d_serapan);
	    $this->db->delete($this->_table);
		
	}

public function tampillaporanserapanH($tgl_awal, $tgl_akhir)
    {

       // $this->db->select("ph.id_h_serapan, ph.kode, ph.nama_program,ph.tanggal, AS akun, SUM(pd.jml_pagu) AS akun, SUM(pd.realisasi) AS realisasi");

     // $this->db->select(' ph.id_h_serapan, ph.kode, ph.nama_program,ph.tanggal,SUM(ph.realisasi) as total_realisasi, SUM(ph.sisa_dana) as sisa_dana ');

       $this->db->select(' ph.id_h_serapan, ph.kode, ph.nama_program,ph.tanggal,pd.jml_pagu, ph.realisasi,ph.sisa_dana');


      $this->db->FROM("serapan_h AS ph"); 
       $this->db->JOIN("pagu AS pd", "ph.akun = pd.akun");
       $this->db->WHERE("ph.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");
       $this->db->GROUP_BY("ph.id_h_serapan");
        $query = $this->db->get();
         
        return $query->result();   

    }



public function tampilDataserapanDPagination($perpage, $uri, $data_pencarian)
	{
		$this->db->select('
		 				   kegiatan.kodekegiatan,
		 				   program.kode,
		 				   program.nama_program,
		 				   pagu.akun,
		 				   serapan.nospp,
		 				   serapan.jenis_perwab,
		 				   serapan.tgl_spp,
		 				   serapan.uraian_pek,
		 				   serapan.nilai_kwit,
		 				   serapan.ppn,
						   serapan.pph21,
						   serapan.pph22,
						   serapan.pph23,
						   serapan.status');
			$this->db->join('kegiatan', ' kegiatan.kodekegiatan = serapan.kodekegiatan');
			$this->db->join('program', ' program.kode = serapan.kode');
			$this->db->join('pagu', 'pagu.akun = serapan.akun');
		if (!empty($data_pencarian)){
			$this->db->like('nospp', $data_pencarian);
		}
		$this->db->order_by('id_d_serapan','asc');
		
		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return $get_data->result();
		}else{
			return null;
		}
	}
	
	public function tombolpagination($data_pencarian)
	{
		//cari jumlah data berdasarkan data pencarian
		$this->db->like('nospp', $data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();
		
		//pagination limit
		$pagination['base_url'] = base_url().'serapanD/listserapanD/load/';
		$pagination['total_rows'] = $hasil;
		$pagination['per_page'] = "3";
		$pagination['uri_segment'] = 4;
		$pagination['num_links'] = 2;
		
		//custom paging configuration
		$pagination['full_tag_open'] = '<div class="paging text-center"><nav><ul class="pagination justify-content-center">';
		$pagination['full_tag_close'] = '</ul></nav></div>';
		
		$pagination['first_link'] = 'First';
		$pagination['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['first_tagl_close'] = '</span></li>';
		
		$pagination['last_link'] = 'Last';
		$pagination['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['last_tagl_close'] = '</span></li>';
		
		$pagination['next_link'] = 'Next';
		$pagination['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
		
		$pagination['prev_link'] = 'Prev';
		$pagination['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['prev_tagl_close'] = '</span></li>';
		
		$pagination['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$pagination['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
		
		$pagination['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['num_tag_close'] = '</span></li>';
		
		$this->pagination->initialize($pagination);
		
		$hasil_pagination = $this->tampilDataserapanDPagination($pagination['per_page'],
		$this->uri->segment(4), $data_pencarian);
		
		return $hasil_pagination;
	}

	public function id_d_serapan()
	{
		$query = $this->db->query("SELECT * FROM serapan ORDER BY id_d_serapan DESC LIMIT 1");
		return $query->result();
	}

public function hitungjmlserapan()
{   
    $query = $this->db->get('serapan');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
}
	

}