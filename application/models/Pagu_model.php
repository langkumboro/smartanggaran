<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Pagu_model extends CI_Model
{
	//panggil nama table
	private $_table = "pagu";
	
public function rulesinput()
	
	{
		return [
		// [
		// 'field' => 'akun',
		// 'label' => 'akun',
		// 'rules' => 'trim|required|max_length[100]|is_unique[pagu.akun]',
		// 'errors' => [
		//    'required' => 'akun tidak boleh Kosong.',
		//    'max_length' => 'akun tidak boleh lebih dari 100 karakter.',
		//    'is_unique'	=> 'akun tidak boleh sama',
		//    ],
		//    ],
		//    [
		//  'field' => 'nama_program',
		//  'label' => 'Nama Program',
		//  'rules' => 'required',
		//  'errors' => [
		//    'required' => 'Nama Perogram tidak boleh kosong.',

		//    ],
		//    ],
            [
         'field' => 'akun',
		'label' => 'Akun',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Akun tidak boleh kosong.',

           	],
           	],
		   [
		'field' => 'jml_pagu',
		'label' => 'Jumlah Pagu',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Jumlah Pagu tidak boleh kosong.',
		  
         		
						]
					],
					
		];
	}
	

	public function rulesedit()
	
	{
		return [
		// [
		// 'field' => 'akun',
		// 'label' => 'akun',
		// 'rules' => 'trim|required|max_length[100]',
		// 'errors' => [
		//    'required' => 'akun tidak boleh Kosong.',
		//    'max_length' => 'akun tidak boleh lebih dari 100 karakter.',
		//    ],
		//    ],
		//    [
		//  'field' => 'nama_program',
		//  'label' => 'Nama Program',
		//  'rules' => 'required',
		//  'errors' => [
		//    'required' => 'Nama Perogram tidak boleh kosong.',

		//    ],
		//    ],
            [
         'field' => 'akun',
		'label' => 'Akun',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Akun tidak boleh kosong.',

           	],
           	],
		   [
		'field' => 'jml_pagu',
		'label' => 'Jumlah Pagu',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Jumlah Pagu tidak boleh kosong.',
		  
          		
						]
					],
					
		];
		
	}

  
	public function tampilDataPagu()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDataPagu2()
	
	{
		$query = $this->db->query("select * from program  as pg inner join pagu as pu on pg.kode=pu.kode");
		// $query = $this->db->query("SELECT * FROM pagu as pu
		// 								LEFT JOIN program AS pr ON pu.kode=sh.kode
		// 								LEFT JOIN program AS pm ON pu.kode=pm.nama_program
		// 								ORDER BY akun ASC");	





			return $query->result();
	}
	
	public function tampilDataPagu3()
	
	{
		$this->db->select('*');
		$this->db->order_by('akun', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
public function save()

	{
		

		$akun_pagu = $this->input->POST('akun');
		$sql = $this->db->query("SELECT akun from pagu where akun = '$akun_pagu'");
		$cek_id = $sql->num_rows();
		if ($cek_id > 0)
		{
			$this->session->set_flashdata('akun');
			redirect('pagu/inputpagu');
		}else{
			$akun_pagu = $this->input->post('akun');
		}
		
		// $data['akun'] =$akun_pagu;
		// $data['nama_program'] =$this->input->post('nama_program');
		$data['akun'] =$akun_pagu;
		$data['jml_pagu'] =$this->input->post('jml_pagu');
		$data['kode'] =$this->input->post('kode');
		$data['nama_program']=$this->input->post('nama_program');
		
		$this->db->insert($this->_table, $data);
	}



	public function detail($akun)
	{
		$this->db->select('*');
		$this->db->where('akun', $akun);
		$result = $this->db->get($this->_table);
		return $result->result();
	}


	public function delete($akun)
	
	{
		
		$this->db->where('akun', $akun);
	    $this->db->delete($this->_table);
		
	}

	public function update($akun)
	{
		
		
		// $data['nama_program'] =$this->input->post('nama_program');
		$data['akun'] =$this->input->post('akun');
		$data['jml_pagu'] =$this->input->post('jml_pagu');
		
		$this->db->where('akun', $akun);
		$this->db->update($this->_table, $data);
	
	}
public function hitungjmlpagu()
{   
    $query = $this->db->get('pagu');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
}

	
	
public function tampilDataPaguPagination($perpage, $uri, $data_pencarian)
{
	$this->db->select('*');
	if (!empty($data_pencarian)) {
		$this->db->like('nama_program', $data_pencarian);
		}
		$this->db->order_by('akun','asc');
		
		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return $get_data->result();
			
			}else{
				return null;
			}	
}

	public function tombolpagination($data_pencarian)
{
	$this->db->like('nama_program', $data_pencarian);
	$this->db->from($this->_table);
	$hasil = $this->db->count_all_results();
	
	//pagination limt
	$pagination['base_url'] = base_url().'pagu/listpagu/load/';
	$pagination['total_rows'] =$hasil;
	$pagination['per_page'] = "3";
	$pagination['uri_segment'] = 4;
	$pagination['num_links'] = 2;
	
	
	$pagination['full_tag_open'] = '<div class="paging text-center"><nav><ul class="pagination justify-content-center">';
		$pagination['full_tag_close'] = '</ul></nav></div>';
		
		$pagination['first_link'] = 'First';
		$pagination['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['first_tagl_close'] = '</span></li>';
		
		$pagination['last_link'] = 'Last';
		$pagination['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['last_tagl_close'] = '</span></li>';
		
		$pagination['next_link'] = 'Next';
		$pagination['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
		
		$pagination['prev_link'] = 'Prev';
		$pagination['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['prev_tagl_close'] = '</span></li>';
		
		$pagination['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$pagination['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
		
		$pagination['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['num_tag_close'] = '</span></li>';
	
	$this->pagination->initialize($pagination);
	
	$hasil_pagination = $this->tampilDataPaguPagination($pagination['per_page'],
	$this->uri->segment(4), $data_pencarian);
	
	return $hasil_pagination;
	
	}




	public function TampilJumlahPagu($akun)
	{
		$query = $this->db->query("SELECT * FROM " . $this->_table . "
			WHERE akun ='$akun'");
		$hasil = $query->result();

		foreach ($hasil as $data) {
			$hasil_jml_pagu = $data->jml_pagu;
		}
		return $hasil_jml_pagu;
	}





}
