<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	date_default_timezone_set('Asia/Jakarta');
	class Model_ku extends CI_Model
	{
		function __construct() // untuk awalan membuat class atau lawan kata nya index
		{
			parent::__construct(); 
		}
		
//-----------------------------------//
//-------DATABASE PERTAMA ----------//
//---------------------------------//
		
		function tampil_data($whr, $ord, $slc, $tbl)
		{	
			$this->db->where($whr);	
			$this->db->order_by($ord);	
			$this->db->select($slc);	
			$qr= $this->db->get($tbl);
			return $qr->result();
		}
		function tampil_table($tbl)
		{
			$qr= $this->db->get($tbl);
			return $qr->result();
		}
		function tampilquery($qr){
			$db = $this->db->query($qr);
			return $db->result();
		}
		
		function insertdata($table, $query, $hal){
			$this->db->insert($table, $query);
			if($hal != ''){
				redirect($hal,'refresh');
			}
		}
		function insertdata_2($table, $query, $hal){
			$this->db2->insert($table, $query);
			if($hal != ''){
				redirect($hal,'refresh');
			}
		}
		function hapusdata($table,$whr,$hal){
			$this->db->delete($table,$whr);
			if($hal != ''){
				redirect($hal,'refresh');
			}
		}
		function update_query($query, $hal){
			$this->db->query($query);
			if($hal != ''){
				redirect($hal,'refresh');
				}
		}
		function updatedata($tbl,$idfield,$value,$query,$hal){
			$this->db->where($idfield,$value);
			$this->db->update($tbl,$query);
			if($hal != ''){
				redirect($hal,'refresh');
				}
		}

	}