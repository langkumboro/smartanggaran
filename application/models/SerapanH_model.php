<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class SerapanH_model extends CI_Model
{
	//panggil nama table
	private $_table = "serapan_h";
	
	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("Pagu_model");
    }

	
public function rulesinput()
	
	{
		return [
		[
		
		 'field' => 'kode',
		 'label' => 'Jenis',
		 'rules' => 'required',
		 'errors' => [
		   'required' => 'Jenis tidak boleh kosong.',
           
       
           ]
		   ]
		  		 ];
	}

public function rules1()
{
        return[
        [
                'field' =>'jenis_komoditas',
                'label' =>'jenis komoditas',
                'rules' =>'required',
                'errors' =>[
                    'required' => 'jenis komoditas tidak boleh kosong.',
                    
                ]
        
        ],
        [
                'field' =>'jumlah',
                'label' =>'jumlah',
                'rules' =>'required|numeric',
                'errors' =>[
                    'required' => 'jumlah tidak boleh kosong.',
                    'numeric' => 'jumlah harus angka.',
                    
                ]
        ],
        [
                'field' =>'harga',
                'label' =>'Harga ',
                'rules' =>'required|numeric',
                'errors' =>[
                    'required' => 'Harga  tidak boleh kosong.',
                    'numeric' => 'Harga  harus angka.',


                ]

        ]
        ];
    }
	
	public function tampilDataSerapan()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDataSerapan2()
	
	 {
        $query	= $this->db->query(
            "SELECT A.*, C.akun, C.jml_pagu, B.kode, B.nama_program FROM " . $this->_table . " 
            	  AS A 
            	  INNER JOIN pagu AS C ON A.akun = C.akun
            	  INNER JOIN program AS B ON C.kode = B.kode
            	 
             WHERE A.flag = 1");
        
        return $query->result();	
    }

	
	public function tampilDataSerapan3()
	
	{
		$this->db->select('*');
		$this->db->order_by('id_h_serapan', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}

	
public function saveSerapanH()
    {
    	// echo "<pre>";
     //    print_r($this->Tampiljmlpagu); die();
     //    echo "</pre>";

        $id = $this->input->POST('id_h_serapan');
		$sql = $this->db->query("SELECT id_h_serapan from serapan_h where id_h_serapan = '$id'");
		$cek_id = $sql->num_rows();
		if ($cek_id > 0)
		{
			$this->session->set_flashdata('id_h_serapan');
			redirect('serapanH/listserapanH');
		}else{
			$id = $this->input->post('id_h_serapan');
		}
		//$tgl_gabung = $thn . "" .$bln . "" . $tgl;
		$sql = $this->db->query("SELECT jml_pagu from pagu where akun = '$jml_pagu'");
		$akun    = $this->input->post('akun'); // ini dia ga dapet jumlah pagu nya geng.. ini isinya
		
		$jml_pagu       = $this->Pagu_model->TampilJumlahPagu($akun);
		$realisasi  = $this->input->post('realisasi');

		// $hasil_jml_pagu = $this->input->post('jml_pagu');

		
		$total_pengurangan = (float)$jml_pagu - (float)$realisasi;
 // echo "$hasil_jml_pagu";
     //    print_r($this->Tampiljmlpagu); die();
     //    echo "</pre>";
/*var_dump($variabel); die();*/
		$data['id_h_serapan'] =$id;
        $data['kode']    = $this->input->post('kode');
        $data['nama_program']    = $this->input->post('nama_program');
        $data['tanggal']        = date('Y-m-d');
        $data['realisasi']          = $realisasi;
		$data['akun']            = $akun;
        $data['sisa_dana']         = (float)$total_pengurangan;
        $data['flag']           = 1;
			

		$this->db->insert($this->_table, $data);
    }


public function delete($id_h_serapan)
	
	{
		
		$this->db->where('id_h_serapan', $id_h_serapan);
	    $this->db->delete($this->_table);
		
	}

public function tampillaporanserapanH($tgl_awal, $tgl_akhir)
    {

       // $this->db->select("ph.id_h_serapan, ph.kode, ph.nama_program,ph.tanggal, AS akun, SUM(pd.jml_pagu) AS akun, SUM(pd.realisasi) AS realisasi");

     // $this->db->select(' ph.id_h_serapan, ph.kode, ph.nama_program,ph.tanggal,SUM(ph.realisasi) as total_realisasi, SUM(ph.sisa_dana) as sisa_dana ');

       $this->db->select(' ph.id_h_serapan, ph.kode, ph.nama_program,ph.tanggal,pd.akun,
       	pd.jml_pagu,pd.akun, ph.realisasi,ph.sisa_dana');


      $this->db->FROM("serapan_h AS ph"); 
       $this->db->JOIN("pagu AS pd", "ph.akun = pd.akun");
       $this->db->WHERE("ph.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");
       $this->db->GROUP_BY("ph.id_h_serapan");
        $query = $this->db->get();
         
        return $query->result();   

    }



public function tampilDataserapanHPagination($perpage, $uri, $data_pencarian)
	{
		$this->db->select('serapan_h.id_h_serapan, program.kode, program.nama_program,pagu.akun, pagu.jml_pagu, serapan_h.realisasi, serapan_h.sisa_dana');
			$this->db->join('program', 'program.kode = serapan_h.kode');
			$this->db->join('pagu', 'pagu.akun = serapan_h.akun');
		if (!empty($data_pencarian)){
			$this->db->like('realisasi', $data_pencarian);
		}
		$this->db->order_by('id_h_serapan','asc');
		
		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return $get_data->result();
		}else{
			return null;
		}
	}
	
	public function tombolpagination($data_pencarian)
	{
		//cari jumlah data berdasarkan data pencarian
		$this->db->like('realisasi', $data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();
		
		//pagination limit
		$pagination['base_url'] = base_url().'serapanH/listserapanH/load/';
		$pagination['total_rows'] = $hasil;
		$pagination['per_page'] = "3";
		$pagination['uri_segment'] = 4;
		$pagination['num_links'] = 2;
		
		//custom paging configuration
		$pagination['full_tag_open'] = '<div class="paging text-center"><nav><ul class="pagination justify-content-center">';
		$pagination['full_tag_close'] = '</ul></nav></div>';
		
		$pagination['first_link'] = 'First';
		$pagination['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['first_tagl_close'] = '</span></li>';
		
		$pagination['last_link'] = 'Last';
		$pagination['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['last_tagl_close'] = '</span></li>';
		
		$pagination['next_link'] = 'Next';
		$pagination['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
		
		$pagination['prev_link'] = 'Prev';
		$pagination['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['prev_tagl_close'] = '</span></li>';
		
		$pagination['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$pagination['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
		
		$pagination['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['num_tag_close'] = '</span></li>';
		
		$this->pagination->initialize($pagination);
		
		$hasil_pagination = $this->tampilDataserapanHPagination($pagination['per_page'],
		$this->uri->segment(4), $data_pencarian);
		
		return $hasil_pagination;
	}

	public function id_h_serapan()
	{
		$query = $this->db->query("SELECT * FROM serapan_h ORDER BY id_h_serapan DESC LIMIT 1");
		return $query->result();
	}

public function hitungjmlserapan()
{   
    $query = $this->db->get('serapan_h');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
}
	

}