<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
	//panggil nama table
	private $_table = "login";
	
	public function cekUser($username, $password)
	{
		$this->db->select('akun,tipe');
		$this->db->where('akun', $username);
		$this->db->where('password', md5($password));
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->row_array();
	}
	public function saveregistrasi(){

		$data['id_user'] =$id_user;
		$data['akun'] =$this->input->post('akun');
		$data['password'] =md5($this->input->post('password'));
		$data['tipe'] =$this->input->post('tipe');
		$data['flag'] =1;
		$this->db->insert($this->_table, $data);

	}
	public function rulesregitrasi()
{
        return[
        [
                'field' =>'akun',
                'label' =>'akun',
                'rules' =>'required',
                'errors' =>[
                    'required' => 'isi username.',
                    
                ]
        
        ],
        [
                'field' =>'password',
                'label' =>'password',
                'rules' =>'required',
                'errors' =>[
                    'required' => 'isi password.',
                    
                    
                ]
        ],
        [
                'field' =>'tipe',
                'label' =>'tipe ',
                'rules' =>'required|numeric',
                'errors' =>[
                    'required' => 'tipe  tidak boleh kosong.',
                    'numeric' => 'tipe  harus angka.',


                ]

        ]
        ];
    }

}


