<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Kegiatan_model extends CI_Model
{
	//panggil nama table
	private $_table = "kegiatan";
	
public function rules()
	
	{
		return [
		[ 
						'field'=> 'kodekegiatan',
						'label' => 'kodekegiatan',
						'rules' => 'required|max_length[200]',
						'errors' => [
							'requered' => 'kodekegiatan tidak boleh kosong.',
							'max_length' => 'kodekegiatan tidak boleh lebih dari 50 karakter.',
						]
					],
		    [
		'field' => 'tgl_pel',
		'label' => 'tgl_pel',
		'rules' => 'required',
		'errors' => [
		   'required' => 'tgl_pel tidak boleh kosong.',

           		   ],
		   ],
            [
         'field' => 'lokasi',
		'label' => 'lokasi',
		'rules' => 'required',
		'errors' => [
		   'required' => 'lokasi tidak boleh kosong.',

           	],
           	],
		   [
		'field' => 'judulkegiatan',
		'label' => 'judulkegiatan',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Judul kegiatan tidak boleh kosong.',
		  
         		
						]
					],
					
		];
	}

public function tampilDataKegiatan()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDataKegiatan2()
	
	{
		$query = $this->db->query("SELECT * FROM kegiatan");
		return $query->result();
	}
	
	public function tampilDataKegiatan3()
	
	{
		$this->db->select('*');
		$this->db->order_by('kodekegiatan', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}

	public function save()

	{
		

		$tgl_pel = $this->input->post('tgl_pel');

		$kodekegiatan = $this->input->POST('kodekegiatan');
		$sql = $this->db->query("SELECT kodekegiatan from kegiatan where kodekegiatan = '$kodekegiatan'");
		$cek_kodekegiatan = $sql->num_rows();
		if ($cek_kodekegiatan > 0)
		{
			$this->session->set_flashdata('kodekegiatan');
			redirect('kegiatan/inputkegiatan');
		}else{
			$kodekegiatan = $this->input->post('kodekegiatan');
		}
		

		$data['kodekegiatan'] =$kodekegiatan;
		$data['tgl_pel'] =$tgl_pel;
		$data['lokasi'] =$this->input->post('lokasi');
		$data['judulkegiatan'] =$this->input->post('judulkegiatan');
		$this->db->insert($this->_table, $data);
	}
	


public function detailkegiatan($kodekegiatan)
	{
		$this->db->select('*');
		$this->db->where('kodekegiatan', $kodekegiatan);
		$result = $this->db->get($this->_table);
		return $result->result();
	}

public function update($kodekegiatan)
	{
		
	
		$data['tgl_pel'] =$this->input->post('tgl_pel');
		$data['lokasi'] =$this->input->post('lokasi');
		$data['judulkegiatan'] =$this->input->post('judulkegiatan');
		
		$this->db->where('kodekegiatan', $kodekegiatan);
		$this->db->update($this->_table, $data);	
	}
public function delete($kodekegiatan)
	
	{
		
		$this->db->where('kodekegiatan', $kodekegiatan);
	    $this->db->delete($this->_table);
		
	}
public function hitungjmlkegiatan()
{   
    $query = $this->db->get('kegiatan');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
}
	
public function tampilDataKegiatanPagination($perpage, $uri, $data_pencarian)
{
	$this->db->select('*');
	if (!empty($data_pencarian)) {
		$this->db->like('lokasi', $data_pencarian);
		}
		$this->db->order_by('kodekegiatan','asc');
		
		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return $get_data->result();
			
			}else{
				return null;
			}	
}

	
	
	
	public function tombolpagination($data_pencarian)
{
	$this->db->like('lokasi', $data_pencarian);
	$this->db->from($this->_table);
	$hasil = $this->db->count_all_results();
	
	//pagination limt
	$pagination['base_url'] = base_url().'kegiatan/listkegiatan/load/';
	$pagination['total_rows'] =$hasil;
	$pagination['per_page'] = "3";
	$pagination['uri_segment'] = 4;
	$pagination['num_links'] = 2;
	
	
	$pagination['full_tag_open'] = '<div class="paging text-center"><nav><ul class="pagination justify-content-center">';
		$pagination['full_tag_close'] = '</ul></nav></div>';
		
		$pagination['first_link'] = 'First';
		$pagination['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['first_tagl_close'] = '</span></li>';
		
		$pagination['last_link'] = 'Last';
		$pagination['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['last_tagl_close'] = '</span></li>';
		
		$pagination['next_link'] = 'Next';
		$pagination['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
		
		$pagination['prev_link'] = 'Prev';
		$pagination['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['prev_tagl_close'] = '</span></li>';
		
		$pagination['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$pagination['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
		
		$pagination['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$pagination['num_tag_close'] = '</span></li>';
	
	$this->pagination->initialize($pagination);
	
	$hasil_pagination = $this->tampilDataKegiatanPagination($pagination['per_page'],
	$this->uri->segment(4), $data_pencarian);
	
	return $hasil_pagination;
	
	}
public function createKodeUrut(){
	//cek kode barang terakhir
	$this->db->select('MAX(kodekegiatan) as kodekegiatan');
	$query  = $this->db->get($this->_table);
	$result = $query->row_array(); //hasil bentuk array

	$kode_terakhir = $result['kodekegiatan'];
	//format BR001 = BR (label awal), 001 (nomor urut)
	$label = "000";
	$no_urut_lama = (int) substr($kode_terakhir, 3,4);
	$no_urut_lama ++;

	$no_urut_baru = sprintf("%03s", $no_urut_lama);
	$kode_baru = $label . $no_urut_baru;

	return $kode_baru;
}
}
