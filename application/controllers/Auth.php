<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("user_model");
	}
	
	public function index()
	{
		//cek login akses
		$user_login	= $this->session->userdata();
		if(count($user_login) <= 1){
			$this->login();
		}else{
			redirect("home/");
		}
	}
	
	public function login()
	{
		if (!empty($_REQUEST)) {
			//ambil dari form login
			$username 	= $this->input->post('username');
			$password 	= $this->input->post('password');
			$data_login = $this->user_model->cekUser($username, $password);
			
			//echo"<pre>";
			//print_r($data_login); die();
			//echo"</pre>";
			
			$data_sesi = array(
				'username' 	=>$data_login['akun'],
				'tipe'		=>$data_login['tipe'],
				'status' 	=> "login"
			);
			
			if(!empty($data_login)) {
				//login berhasil
				$this->session->set_userdata($data_sesi);
				redirect("home/", "refresh");
			}else{
				//login gagal
				$this->session->set_flashdata('info', '<div style="color: red" align="center"> Username dan Password Salah </div>');
				redirect("auth/", "refresh");
			}
		}
		$this->load->view('Home_login');
	}
	public function registrasi()
	
	{
		
		$data['content'] = 'forms/registrasi';
	
		
		$validation = $this->form_validation;
		$validation->set_rules($this->user_model->rulesregitrasi());
		
		if ($validation->run()) {
			$this->user_model->saveregistrasi();
			$this->session->set_flashdata('info', '<div style="color : green">Alhamdulilah Registrasi Berhasil</div>');
			// redirect("pegawai/index", "refresh");
            redirect("auth/login");
			}
			
		$this->load->view('forms/registrasi', $data);
	
}
	
	/*public function home()
	{
		$this->load->view('welcome_massage_table');
	}
	*/
	public function logout()
	{
		$this->session->sess_destroy();
		redirect("auth/", "refresh");
	}
}