<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class SerapanH extends CI_controller {


	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("SerapanH_model");
		$this->load->model("Pagu_model");
		$this->load->model("Program_model");
		

		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	$this->load->library("pdf/pdf");
	}
	
	public function index()
	
	{
		$this->listserapanH();
	}
	
	// public function listserapanH($id_h_serapan='')
	public function listserapanH()
	{
	// {
		// $data['id_h_serapan'] = $id_h_serapan;
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarianserapanH', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarianserapanH');
			
		}

       
		$data['data_serapanH'] = $this->SerapanH_model->tombolpagination($data['kata_pencarian']);
		// $data['data_serapanH'] = $this->SerapanH_model->tampilDataSerapan2();
 		$data['data_pagu'] = $this->Pagu_model->tampilDataPagu();
 		$data['totalserapan'] = $this->SerapanH_model->hitungjmlserapan();
		$data['data_program'] = $this->Program_model->tampilDataProgram();


		
		$data['content']  ='forms/list_serapan';

		$this->load->view('home_2', $data);
	}

	
	public function inputSerapan()
	
	{
		$id = $this->input->post('id_h_serapan');
		$data['data_pagu'] = $this->Pagu_model->tampilDataPagu();
		$data['data_program'] = $this->Program_model->tampilDataProgram();
		$data['content'] = 'forms/input_serapanH';
	
		
		$validation = $this->form_validation;
		$validation->set_rules($this->SerapanH_model->rulesinput());
		
		if ($validation->run()) {
			$this->SerapanH_model->saveSerapanH();
			$this->session->set_flashdata('info', '<div style="color : green">Simpan Data Berhasil !</div>');
			redirect("serapanH/index");
			}
			
		
		$this->load->view('home_2', $data);
	
}

public function deleteserapan($id_h_serapan)
	{
		$m_serapanH = $this->SerapanH_model;
		$m_serapanH->delete($id_h_serapan);
		// redirect("pegawai/index", "refresh");
        redirect("serapanH/index");
	}

 public function report()  {
    
    $data['content']    ='forms/report';
    $this->load->view('home_2', $data);    
   }
   public function laporan()  
    {

                    // echo "<prev>";
                    //     print_r($this->input->post('tanggal_awal'));die();
                    // echo "</prev>";
        // if (!empty($_REQUEST)) {
                    $tgl_awal   = $this->input->post('tgl_awal');
                    $tgl_akhir   = $this->input->post('tgl_akhir');
                    $data['data_serapanH']  = $this->SerapanH_model->tampillaporanserapanH($tgl_awal, $tgl_akhir);
                    $data['tgl_awal'] = $tgl_awal;
                    $data['tgl_akhir'] = $tgl_akhir;

                    $data['content']                ='forms/laporan';
                    $this->load->view('home_2', $data);
                
                    

        // }else{
            // redirect("Pembelian/laporan/", "refresh");
    
       // }
   }
function cetakpdf($tgl_awal, $tgl_akhir){
        $pdf = new FPDF('L','mm','Letter');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',12);
        // mencetak string 
        $pdf->Cell(250,7,'MONITORING & SERAPAN ANGGARAN',0,1,'C');
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(250,7,'Laporan Hasil Serapan',0,1,'C');
        $pdf->Cell(250,7,$tgl_awal.' s.d '.$tgl_akhir,0,1,'C');
        $pdf->Cell(25,20,'Atas Nama :',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(250,6,'',0,1);
        $pdf->SetFont('Arial','B',12,'C');
        $pdf->Cell(40,6,'Kode Program',1,0,'C');
        $pdf->Cell(60,6,'Akun',1,0,'C');
  		$pdf->Cell(40,6,'Tanggal',1,0,'C');
        $pdf->Cell(40,6,'Pagu [IDR]',1,0,'C');
        $pdf->Cell(40,6,' Realisasi',1,0,'C');
        $pdf->Cell(40,6,'Dana Sisa',1,1,'C');
        


        $pdf->SetFont('Arial','B',11);
        $no     = 0;
        
        $laporan = $this->SerapanH_model->tampillaporanserapanH($tgl_awal, $tgl_akhir);
        


        foreach($laporan as $data){
            $no ++;
            $pdf->Cell(40,6,$data->kode,1,0,'C');
            // $pdf->Cell(30,6,$data->nama_program,1,0,'C');
            $pdf->Cell(60,6,$data->akun,1,0,'J');
            $pdf->Cell(40,6,$data->tanggal,1,0,'C');
            $pdf->Cell(40,6,'Rp.'.number_format($data->jml_pagu) ,1,0,'L');
            $pdf->Cell(40,6,'Rp.'.number_format($data->realisasi) ,1,0,'L');
            $pdf->Cell(40,6,'Rp.'.number_format($data->sisa_dana) ,1,1,'L');
           
           
           
        }
        $pdf->SetFont('Arial','B',12);
        // mencetak string 
        $pdf->Cell(400,25,'Tanda Tangan',0,1,'C');
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(400,15,'..........................',0,1,'C');
       
        $pdf->Output();
    }
 
}