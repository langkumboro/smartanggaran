<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Pagu extends CI_controller {

public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("Pagu_model");
        $this->load->model("Program_model");
       
		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	$this->load->library("pdf/pdf");
	}
	
	public function index()
	
	{
		$this->listPagu();


	}
	
	public function listPagu()
	
	{
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarianpagu', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarianpagu');
			
		}

       
		$data['data_pagu'] = $this->Pagu_model->tombolpagination($data['kata_pencarian']);
        // $data['data_pagu'] = $this->Pagu_model->tampilDataPagu2();
 		
		// $data['data_pegawai'] = $this->Pegawai_model->tampilDataPegawai();
         $data['data_program'] = $this->Program_model->tampilDataProgram();
         $data['totalpagu'] = $this->Pagu_model->hitungjmlpagu();
        
		$data['content']       ='forms/list_pagu';
		$this->load->view('home_2', $data);
	}
	public function inputpagu()
	
	{
        $akun_pagu = $this->input->post('akun');

        $data['data_program'] = $this->Program_model->tampilDataProgram(); 
        // $data['data_pagu'] = $this->Pagu_model->tampilDataPagu();
		
		$data['content'] = 'forms/input_pagu';
	
		
		$validation = $this->form_validation;
		$validation->set_rules($this->Pagu_model->rulesinput());
		
		if ($validation->run()) {
			$this->Pagu_model->save();
			$this->session->set_flashdata('info', '<div style="color : green">SAVE DATA SUCCESS</div>');
			// redirect("pegawai/index", "refresh");
            redirect("pagu/index");
			}
			
		$this->load->view('home_2', $data);
	
}


public function editpagu($akun)
    {   
        $data['edit_pagu']   = $this->Pagu_model->detail($akun);
        $data['data_program']       = $this->Program_model->tampilDataProgram();

        $validation = $this->form_validation;
        $validation->set_rules($this->Pagu_model->rulesedit());
        if ($validation->run()){
            $this->Pagu_model->update($akun);
            $this->session->set_flashdata('info', '<div style="color : green">SUCCESS DATA ON UPDATE </div>');
            redirect("pagu/index", "refresh");
            }   
            
        
            $data['content']       ='forms/edit_pagu';
            $this->load->view('home_2', $data); 
    }


	public function deletepagu($akun)
	{
		$m_pagu = $this->Pagu_model;
		$m_pagu->delete($akun);
		// redirect("pegawai/index", "refresh");
        redirect("pagu/index");
	}

    public function exporjson()
    {
        $db = $this->db->query("SELECT * FROM pagu");
        $output = $db->result(); 
        

        $document = new DOMDocument();
        $document->formatOutput = true;

        $root = $document->createElement("data");
        $document->appendChild( $root );

        foreach ( $output as $pagu ){

            $block = $document->createElement( "pagu" );

            $akun = $document->createElement( "akun" );
            $akun->appendChild(
            $document->createTextNode( $pagu->akun ));
            $block->appendChild( $akun );

            $kode = $document->createElement( "kode" );
            $kode->appendChild(
            $document->createTextNode( $pagu->kode ));
            $block->appendChild( $kode );

            $nama_pagu = $document->createElement( "nama_pagu" );
            $nama_pagu->appendChild(
            $document->createTextNode( $pagu->nama_pagu ));
            $block->appendChild( $nama_pagu );

            $jml_pagu = $document->createElement( "jml_pagu" );
            $jml_pagu->appendChild(
            $document->createTextNode( $pagu->jml_pagu ));
            $block->appendChild( $jml_pagu );

            $root->appendChild( $block );
        }

            $document->save("./assets/json/pagu.json");

            $this->listPagu();
            $this->session->set_flashdata('info', '<div style="color : green">Data Export Success</div>');
            redirect("pagu/index");
        }
}