<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class SerapanD extends CI_controller {


	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("SerapanD_model");
		$this->load->model("Pagu_model");
		$this->load->model("Program_model");
		$this->load->model("Kegiatan_model");
		

		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	$this->load->library("pdf/pdf");
	}
	
	public function index()
	
	{
		$this->listserapanD();
	}
	
	// public function listserapanH($id_h_serapan='')
	public function listserapanD()
	{
	// {
		// $data['id_h_serapan'] = $id_h_serapan;
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarianserapanD', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarianserapanD');
			
		}

       
		$data['data_serapanD'] = $this->SerapanD_model->tombolpagination($data['kata_pencarian']);
		// $data['data_serapanH'] = $this->SerapanH_model->tampilDataSerapan2();
 		$data['data_pagu'] = $this->Pagu_model->tampilDataPagu();
		$data['data_program'] = $this->Program_model->tampilDataProgram();
		$data['data_kegiatan'] = $this->Kegiatan_model->tampilDataKegiatan();


		
		$data['content']  ='forms/list_serapanD';

		$this->load->view('home_2', $data);
	}

	
	public function inputSerapanD()
	
	{
		$id = $this->input->post('id_d_serapan');
		$data['data_pagu'] = $this->Pagu_model->tampilDataPagu();
		$data['data_kegiatan'] = $this->Kegiatan_model->tampilDataKegiatan();
		$data['data_program'] = $this->Program_model->tampilDataProgram();
		$data['content'] = 'forms/input_serapanD';
	
		
		$validation = $this->form_validation;
		$validation->set_rules($this->SerapanD_model->rulesinput());
		
		if ($validation->run()) {
			$this->SerapanD_model->saveSerapanD();
			$this->session->set_flashdata('info', '<div style="color : green">Simpan Data Berhasil !</div>');
			redirect("serapanD/index");
			}
			
		
		$this->load->view('home_2', $data);
	
}

public function deleteserapan($id_h_serapan)
	{
		$m_serapanD = $this->SerapanD_model;
		$m_serapanD->delete($id_d_serapan);
		// redirect("pegawai/index", "refresh");
        redirect("serapanD/index");
	}

 public function report()  {
    
    $data['content']    ='forms/report';
    $this->load->view('home_2', $data);    
   }

 public function laporan()  
    {

                    // echo "<prev>";
                    //     print_r($this->input->post('tanggal_awal'));die();
                    // echo "</prev>";
        // if (!empty($_REQUEST)) {
                    $tgl_awal   = $this->input->post('tgl_awal');
                    $tgl_akhir   = $this->input->post('tgl_akhir');
                    $data['data_serapanH']  = $this->SerapanH_model->tampillaporanserapanH($tgl_awal, $tgl_akhir);
                    $data['tgl_awal'] = $tgl_awal;
                    $data['tgl_akhir'] = $tgl_akhir;

                    $data['content']                ='forms/laporan';
                    $this->load->view('home_2', $data);
                
                    

        // }else{
            // redirect("Pembelian/laporan/", "refresh");
    
       // }
   }
function cetakpdf($tgl_awal, $tgl_akhir){
        $pdf = new FPDF('p','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',11);
        // mencetak string 
        $pdf->Cell(187,7,'Monitoring & Serapan Anggaran',0,1,'C');
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(190,7,'Laporan Hasil Serapan Header',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',11,'C');
        $pdf->Cell(30,6,'Kode Program',1,0,'C');
        // $pdf->Cell(30,6,'Nama Program',1,0,'C');
  		$pdf->Cell(30,6,'Tanggal',1,0,'C');
        $pdf->Cell(40,6,'Pagu [IDR]',1,0,'C');
        $pdf->Cell(40,6,' Realisasi',1,0,'C');
        $pdf->Cell(40,6,'Dana Sisa',1,1,'C');
        


        $pdf->SetFont('Arial','B',11);
        $no     = 0;
        
        $laporan = $this->SerapanH_model->tampillaporanserapanH($tgl_awal, $tgl_akhir);
        


        foreach($laporan as $data){
            $no ++;
            $pdf->Cell(30,6,$data->kode,1,0,'C');
            // $pdf->Cell(30,6,$data->nama_program,1,0,'C');
            $pdf->Cell(30,6,$data->tanggal,1,0,'C');
            $pdf->Cell(40,6,'Rp.'.number_format($data->jml_pagu) ,1,0,'L');
            $pdf->Cell(40,6,'Rp.'.number_format($data->realisasi) ,1,0,'L');
            $pdf->Cell(40,6,'Rp.'.number_format($data->sisa_dana) ,1,1,'L');
           
           
           
        }
        
       
        $pdf->Output();
    }
 

}