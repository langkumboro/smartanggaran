<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Program extends CI_controller {


public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("Program_model");

		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	$this->load->library("pdf/pdf");
	}
	
public function index()
	
	{
		$this->listProgram();
	}
	
public function listProgram()
	
	{
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarianprogram', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarianprogram');
			
		}
		
		$data['data_program'] = $this->Program_model->tombolpagination($data['kata_pencarian']);
		$data['totalprogram'] = $this->Program_model->hitungjmlprogram();
 		
		// $data['data_pegawai'] = $this->Pegawai_model->tampilDataPegawai();
		$data['content']       ='forms/list_program';
		$this->load->view('home_2', $data);
	}
public function inputprogram()
	
	{
        $data['data_program'] = $this->Program_model->tampilDataProgram();
		$kode = $this->input->post('kode');
		$data['content'] = 'forms/input_program';
	
		
		$validation = $this->form_validation;
		$validation->set_rules($this->Program_model->rulesinput());
		
		if ($validation->run()) {
			$this->Program_model->save();
			$this->session->set_flashdata('info', '<div style="color : green">SAVE DATA SUCCESS</div>');
			// redirect("pegawai/index", "refresh");
            redirect("program/index");
			}
			
		$this->load->view('home_2', $data);
	
}
public function editprogram($kode)
    {   
        $data['edit_program']   = $this->Program_model->detailprogram($kode);
        

        $validation = $this->form_validation;
        $validation->set_rules($this->Program_model->rulesedit());
        if ($validation->run()){
            $this->Program_model->update($kode);
            $this->session->set_flashdata('info', '<div style="color : green">SUCCESS DATA ON UPDATE </div>');
            redirect("program/index", "refresh");
            }   
            
            $data['content']        = 'forms/edit_program';
            $this->load->view('home_2', $data); 
    }

public function deleteprogram($kode)
	{
		$m_program = $this->Program_model;
		$m_program->delete($kode);
		// redirect("pegawai/index", "refresh");
        redirect("program/index");
	}

public function exportxml()
    {
        $db = $this->db->query("SELECT * FROM program");
        $output = $db->result(); 
        

        $document = new DOMDocument();
        $document->formatOutput = true;

        $root = $document->createElement("data");
        $document->appendChild( $root );

        foreach ( $output as $program ){

            $block = $document->createElement( "program" );

            $kode = $document->createElement( "kode" );
            $kode->appendChild(
            $document->createTextNode( $program->kode ));
            $block->appendChild( $kode );

            $nama_program = $document->createElement( "nama_program" );
            $nama_program->appendChild($document->createTextNode( $program->nama_program ));
            $block->appendChild( $nama_program );

            $root->appendChild( $block );
        }

            $document->save("./assets/xml/program.xml");

            $this->listProgram();
            $this->session->set_flashdata('info', '<div style="color : green">Data Export Success</div>');
            redirect("program/index");
        }



}