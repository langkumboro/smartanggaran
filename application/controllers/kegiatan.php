<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Kegiatan extends CI_controller {


public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("Kegiatan_model");

		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	$this->load->library("pdf/pdf");
	}
	
public function index()
	
	{
		$this->listKegiatan();
	}
	
public function listKegiatan()
	
	{
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencariankegiatan', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencariankegiatan');
			
		}
		
		$data['data_kegiatan'] = $this->Kegiatan_model->tombolpagination($data['kata_pencarian']);
		$data['totalkegiatan'] = $this->Kegiatan_model->hitungjmlkegiatan();

 		
		// $data['data_pegawai'] = $this->Pegawai_model->tampilDataPegawai();
		$data['content']       ='forms/list_kegiatan';
		$this->load->view('home_2', $data);
	}
public function inputkegiatan()
	
	{
        

        $kodekegiatan = $this->input->post('kodekegiatan');
        $data['data_kegiatan'] = $this->Kegiatan_model->tampilDataKegiatan();
        $data['kode_baru'] = $this->Kegiatan_model->createKodeUrut();


  //       $data['data_kegiatan'] = $this->Kegiatan_model->tampilDataKegiatan();
  //       $data['kodekegiatan'] = $this->Kegiatan_model->createKodeUrut();

		// $kode = $this->input->post('kode');
		$data['content'] = 'forms/input_kegiatan';
	
		
		$validation = $this->form_validation;
		$validation->set_rules($this->Kegiatan_model->rules());
		
		if ($validation->run()) {
			$this->Kegiatan_model->save();
			$this->session->set_flashdata('info', '<div style="color : green">SAVE DATA SUCCESS</div>');
			// redirect("pegawai/index", "refresh");
            redirect("kegiatan/index");
			}
			
		$this->load->view('home_2', $data);
    }

public function editkegiatan($kodekegiatan)
    {   
        $data['edit_kegiatan']   = $this->Kegiatan_model->detailkegiatan($kodekegiatan);
        

        $validation = $this->form_validation;
        $validation->set_rules($this->Kegiatan_model->rules());
        if ($validation->run()){
            $this->Kegiatan_model->update($kodekegiatan);
            $this->session->set_flashdata('info', '<div style="color : green">SUCCESS DATA ON UPDATE </div>');
            redirect("kegiatan/index", "refresh");
            }   
            
            $data['content']        = 'forms/edit_kegiatan';
            $this->load->view('home_2', $data); 
    }



public function deletekegiatan($kodekegiatan)
	{
		$m_kegiatan = $this->Kegiatan_model;
		$m_kegiatan->delete($kodekegiatan);
		// redirect("pegawai/index", "refresh");
        redirect("kegiatan/index");
	}
public function exportxml()
    {
        $db = $this->db->query("SELECT * FROM kegiatan");
        $output = $db->result(); 
        

        $document = new DOMDocument();
        $document->formatOutput = true;

        $root = $document->createElement("data");
        $document->appendChild( $root );

        foreach ( $output as $kegiatan ){

            $block = $document->createElement( "kegiatan" );

            $kodekegiatan = $document->createElement( "kodekegiatan" );
            $kodekegiatan->appendChild(
            $document->createTextNode( $kegiatan->kodekegiatan ));
            $block->appendChild( $kodekegiatan );

            $tgl_pel = $document->createElement( "tgl_pel" );
            $tgl_pel->appendChild(
            $document->createTextNode( $kegiatan->tgl_pel ));
            $block->appendChild( $tgl_pel );

            $lokasi = $document->createElement( "lokasi" );
            $lokasi->appendChild(
            $document->createTextNode( $kegiatan->lokasi ));
            $block->appendChild( $lokasi );

            $judulkegiatan = $document->createElement( "judulkegiatan" );
            $judulkegiatan->appendChild(
            $document->createTextNode( $kegiatan->judulkegiatan ));
            $block->appendChild( $judulkegiatan );

            $root->appendChild( $block );
        }

            $document->save("./assets/xml/kegiatan.xml");

            $this->listKegiatan();
            $this->session->set_flashdata('info', '<div style="color : green">Data Export Success</div>');
            redirect("kegiatan/index");
        }


 public function exportjson()
 {
 	$db = $this->db->query("SELECT * FROM kegiatan");
        $output = $db->result(); 


        $data = array();
		foreach ($query as $data) {
			$data = (array)$data;

			$data2['kodekegiatan'] = $data['kodekegiatan'];
			$data2['tgl_pel'] = $data['tgl_pel'];
			$data2['lokasi'] = $data['lokasi'];
			$data2['judulkegiatan'] = $data['judulkegiatan'];
			

			array_push($data, $data2);
		}

		$json = json_encode($data,JSON_PRETTY_PRINT);

		$fileName = $_POST['fileName'].".json";

		$handle = fopen($fileName, 'w');

		$write = fwrite($handle, $json);
		
		$document->save("./assets/json/kegiatan.json");

            $this->listKegiatan();
            $this->message->Create(".$fileName.Is Successful");
            redirect("kegiatan/index");

	}
}