<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Dashboard extends CI_controller {


public function __construct()
	
	{
		
				parent::__construct();
				$this->load->model("Program_model");
				$this->load->model("Pagu_model");
				$this->load->model("Kegiatan_model");
				$this->load->model("SerapanH_model");
					
					//cek sesi login
					$user_login = $this->session->userdata();
					if(count($user_login) <= 1){
					redirect("auth/index", "refresh");
					}
					
				$this->load->library("pdf/pdf");
	}
	
	public function index()
	
	{
		$this->listDashboard();
	}
	
	public function listDashboard()
	
	{
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian');
			
		}
		$data['totalprogram'] = $this->Program_model->hitungjmlprogram();
		$data['totalpagu'] = $this->Pagu_model->hitungjmlpagu();
		$data['totalkegiatan'] = $this->Kegiatan_model->hitungjmlkegiatan();
		$data['totalserapan'] = $this->SerapanH_model->hitungjmlserapan();
		$data['content']       ='forms/list_dasboard';
		$this->load->view('home_2', $data);
	}


}
